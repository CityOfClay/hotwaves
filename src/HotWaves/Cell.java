package HotWaves;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author MultiTool
 */
public class Cell extends CellBase {
  public static double TRate_Basis = 0.005;
//  public double TRate = 0.9;// time rate
//  public double TRate = 0.5;// time rate
//  public double TRate = 0.1;// time rate
//  public double TRate = 0.05;// time rate
  //public double TRate = 0.01;// time rate, normal
//  public double TRate = 0.005;// time rate
//  public double TRate = 0.001;// time rate
//  public double TRate = 0.0001;// time rate
//  public double TRate = 0.00001;// time rate
  //public double TRate_Next = 1.0;// time rate, normal
  public boolean IsWall = false;// eventually we will make this a separate class and override the related methods
  // -----------------------------------------------
//  public double Damp = 1.0; //0.99;
//  public double Tension = 0.0, Speed = 0.0, Speed_Next = 0.0;// Speed is also used as inertia
  public double Basis = 0.0;// average Amp of all my neighbors
//  private double Amp_Prev = 0.0, Amp = 0.0, Amp_Next = 0.0;
  private double MinAmp = Double.POSITIVE_INFINITY, MaxAmp = Double.NEGATIVE_INFINITY;
  double AvgAmp = 0.0, NumRuns = 0.0, SumAmp = 0;
  Vector PoyntingVect = null, PoyntingVectNormed = null;
  private double PoyntingMag;
//  public double Fluid = 0.0, Fluid_Next = 0.0;// fluid flow of grav field  
//  public Vector VectorMass = null, VectorMass_Next = null, VectorGrav = null, VectorGrav_Next = null;
//  public double StaticGrav = 0.0, StaticGrav_Next = 0.0;
  public boolean IsEmitter = false;
  /* ********************************************************************************* */
  public Cell() {
    Init_NbrCross();
    this.VectorMass = new Vector(NDims);
    this.VectorMass_Next = new Vector(NDims);
    this.VectorGrav = new Vector(NDims);
    this.VectorGrav_Next = new Vector(NDims);
    this.PoyntingVect = new Vector(NDims);
    this.PoyntingVectNormed = new Vector(NDims);
  }
  /* ********************************************************************************* */
  @Override public void SetCell(double Value) {// make cell emit a quantity
    Cell cell = this;
    cell.IsEmitter = true;// hacky 
    cell.Fluid = Value;
    cell.VectorMass.Fill(Value);
    cell.VectorMass_Next.Fill(Value);
    cell.VectorGrav.Fill(Value);
    cell.VectorGrav_Next.Fill(Value);
    cell.StaticGrav = 0.0;//Math.abs(Value);
    cell.StaticGrav_Next = 0.0;//Math.abs(Value);
  }
  /* ********************************************************************************* */
  @Override public void Assign_DrawBox(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {
    this.XLoc = XLoc0;
    this.YLoc = YLoc0;
    this.Wdt = Wdt0;
    this.Hgt = Hgt0;
    this.XCtr = this.XLoc + (this.Wdt / 2.0);
    this.YCtr = this.YLoc + (this.Hgt / 2.0);
    VectorMass.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorMass_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
  }
  /* ********************************************************************************* */
  public double Calc_Energy_Component(int Dim) {
    /* 
     If I understand correctly, Falstad seems to calculate the direction of energy flow (Poynting vector) by simply getting the difference 
     between the speeds of two opposite neighbors, and then multiplying by the current amp (height value) of the cell.
     By 'speed' I mean the rate of change of the altitude of the medium in each cell. 
     */
    double Component = 0.0;
    CellBase[] nbrs = this.NbrCross[Dim];
    CellBase nbr0 = nbrs[0];// NAxis
    CellBase nbr1 = nbrs[1];
    if (nbr0 != null && nbr1 != null) {// if energy ls flowing from nbr0 to nbr1, return value will be positive
      Component = nbr1.Get_Speed() - nbr0.Get_Speed();// at this point the value is proportionate for any wavelength, but not the correct absolute magnitude. The vector will point in the right direction but with the wrong length.
    }
    return Component;
  }
  /* ********************************************************************************* */
  public void Calc_Energy_Vector(Vector PoyntingVect) {
    /*
    to do: Calc_Energy_Component to get E vector, then normalize E vector with pythag.
    then get energy of cell as abs(delta height (speed?)) and multiply that by E vector. 
     */
    double SumSq = 0.0;
    double Mag;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      Mag = this.Calc_Energy_Component(DimCnt);
      PoyntingVect.Set(DimCnt, Mag);
      SumSq += Mag * Mag;
    }
    this.PoyntingVectNormed.Copy_From(PoyntingVect);
    this.PoyntingVectNormed.Normalize();
    this.PoyntingMag = Math.sqrt(SumSq);// pythagorean
//    double hypot = this.PoyntingMag;
//    if (Math.abs(hypot) < Globals.Fudge) {
//      hypot = Globals.Fudge * Math.signum(hypot);
//    }
//    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {// normalize length
//      this.PoyntingVectNormed.Set(DimCnt, PoyntingVect.Get(DimCnt) / hypot);// normalized 
//    }
//    double MyAmp = this.Get_Amp();
    double Energy = Math.abs(this.Amp - this.Amp_Prev);
    if (Math.abs(Energy) < Globals.Fudge) {
      Energy = Globals.Fudge * Math.signum(Energy);
    }
//    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {// amplify length
//      this.PoyntingVect[DimCnt] = this.PoyntingVectNormed[DimCnt] * Energy;// tied to change 
//    }
  }
  /* ********************************************************************************* */
  public static double Contrast_Energy_Vectors(Vector MyVectMass, Vector YourVectGrav) {
    Vector Temp = new Vector(NDims);
    Temp.Copy_From(MyVectMass);
    Temp.Normalize();// my direction matters to how I feel your gravity, but my mass does not. 
    Temp.Subtract(YourVectGrav);
    return Temp.Magnitude();// magnitude of difference between vectors

    // Assuming both vectors have been normalized to length 1
//    double DotProd = 0.0;// first get the dot product
//    for (int cnt = 0; cnt < NDims; cnt++) {  DotProd += Vect0[cnt] * Vect1[cnt]; }
//    double Radius = 1.0;
//    double Metric = Radius - DotProd;// if DotProd is 1, becomes 0.  If DotProd is -1, becomes 2.  
//    Metric /= 2.0;// normalize to range 0.0 to 1.0.  This gives cartoon relativistic velocity ratio
//    return Metric;
  }
  /* ********************************************************************************* */
  @Override public double Get_Amp() {
    return this.Amp;
  }
  /* ********************************************************************************* */
  @Override public void Set_Amp(double Amp0) {
    this.Amp = Amp0;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  public void Set_Wallness(double Damp0) {
    if (Damp0 < 1.0) {
      this.IsWall = true;
      if (Double.isInfinite(Damp0)) {
        System.out.println();
      }
      this.Damp = Damp0;
//    this.TRate = 0.0;
//      this.DisconnectAll();
    } else {
      this.IsWall = false;
    }
  }
  /* ********************************************************************************* */
  public void Check_Amp() {
    if (this.MaxAmp < this.Amp) {
      this.MaxAmp = this.Amp;
    }
    if (this.MinAmp > this.Amp) {
      this.MinAmp = this.Amp;
    }
  }
  /* ********************************************************************************* */
  public double Calc_Basis(double DefaultBasis) {// Basis is just average of all neighbors. My amp is attracted to this value.
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    double Result = DefaultBasis;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.Get_Amp();
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      //Sum += this.Amp; NNbrs++;
      Result = Sum / NNbrs;// 4 neighbors in 2 dimensions
    }
    return Result;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Fluid() {// get average fluid height for diffusion test
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.Fluid;
          NNbrs++;
        }
      }
    }
    double AvgFluid = 0;
    if (NNbrs > 0) {
      AvgFluid = Sum / NNbrs;// 4 neighbors in 2 dimensions
    }
    return AvgFluid;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Tension(double Base) {
    return Base - this.Amp;// pull toward avg amp of neighbors
  }
  /* ********************************************************************************* */
  public double Calc_Speed() {
//    return (this.Amp - this.Prev_Amp)/this.TRate;// vector toward future state
    return (this.Amp - this.Amp_Prev) / 1.0;// vector toward future state
  }
  /* ********************************************************************************* */
  public void Average_VGrav(Vector Result) {
    Result.Clear();// get average of all neighbors' vector gravities
    double NNbrs = 0;
    CellBase nbr;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Result.Add(nbr.VectorGrav);
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      Result.Multiply(1.0 / NNbrs);// divide sum to get average
    }
  }
  /* ********************************************************************************* */
  public double AvgNbrStaticGrav() {// get average of all neighbors' static gravities
    double Sum = 0.0;
    double NNbrs = 0;
    CellBase nbr;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.StaticGrav;
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      Sum /= NNbrs;// 4 neighbors in 2 dimensions
    }
    return Sum;
  }
  /* ********************************************************************************* */
  public double Average_NbrVGravMagnitudes() {// get average of all neighbors' vector gravity magnitudes
    double Sum = 0.0;
    double NNbrs = 0;
    CellBase nbr;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.VectorGrav.Magnitude();
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      Sum /= NNbrs;// 4 neighbors in 2 dimensions
    }
    return Sum;
  }
  /* ********************************************************************************* */
  public void Calc_Amp(double OtherAmp) {// Calc_Amp and Rollover from bench testing.  this works.
    double Delta = OtherAmp - this.Amp;// Spring force
    Delta *= TRateSquared;
    this.Amp_Next = this.Amp + this.Speed;
    this.Amp_Next = this.Amp_Next + Delta;
    this.Speed_Next = this.Amp_Next - this.Amp;
  }
  public void Rollover2() {
    this.Amp_Prev = this.Amp;
    this.Amp = this.Amp_Next;
    this.Speed = this.Speed_Next;
  }
  /* ********************************************************************************* */
  @Override public void Update() {// getting close to runcycle
    // look for better alg:
    // https://www.gamedev.net/resources/_/technical/graphics-programming-and-theory/the-water-effect-explained-r915
    this.Basis = this.Calc_Basis(this.Basis);
    this.Tension = this.Calc_Tension(this.Basis);
    double GravLeakRate = 0.9999, GravDrag = 1.0 - GravLeakRate;
    this.Fluid_Next = (this.Fluid * GravDrag) + (this.Calc_Fluid() * GravLeakRate);// gravity flow
    /*
    So next amp is
    this amp plus its current speed, damped.
    plus the tension toward the average of neighbors, limited by timerate.
     */

    if (false) {
//      this.TRate = 0.7;
      this.Speed = this.Speed_Next = this.Calc_Speed();
      this.Amp_Next = this.Amp + ((this.Speed * this.Damp) + (this.Tension * TRate));// snox not sure about this.
    } else {
//      this.TRate = 0.001;//0.7;
      this.Speed = this.Speed_Next = this.Calc_Speed() / this.TRate;
      double Next_Amp_Full = this.Amp + ((this.Speed * this.Damp) + (this.Tension));// attempt to slow down time rate
      double AmpRange = Next_Amp_Full - this.Amp;// linear chop
      this.Amp_Next = this.Amp + (AmpRange * this.TRate);
    }

    this.Calc_Energy_Vector(this.PoyntingVect);

    if (false) {// HotWaves
      this.Average_VGrav(this.VectorGrav_Next);
      double VGravMag = this.VectorGrav_Next.Magnitude();
      double NbrFullPotentialVgravMag = this.Average_NbrVGravMagnitudes();// AvgNbrVGravMags

      // but do we do this on vgravnext, vgravnow or after rollover? 
      double StagnatingGravNext = 0;
      StagnatingGravNext = NbrFullPotentialVgravMag - VGravMag;// Stagnating grav - amount we are going to add to our local static gravity
      //System.out.println("SG:" + StagnatingGravNext);
      double AvgNbrStaticGravNext = 0;
      AvgNbrStaticGravNext = AvgNbrStaticGrav();
      this.StaticGrav_Next = (AvgNbrStaticGravNext + StagnatingGravNext) * 1.0;//0.5;//0.9;// snox does averaging here make sense? 
    }

    // statistics
    this.SumAmp += this.Amp;
    this.NumRuns++;
    this.AvgAmp = this.SumAmp / this.NumRuns;
  }
  /* ********************************************************************************* */
  @Override public void Rollover() {// set up for next cycle
    this.Amp_Prev = this.Amp;
    this.Amp = this.Amp_Next;
    this.Fluid = this.Fluid_Next;
    this.Speed = this.Speed_Next;
    this.StaticGrav = this.StaticGrav_Next;
    this.VectorGrav.Copy_From(this.VectorGrav_Next);
    if (false) {
      this.Set_TRate(this.TRate_Next);
    }
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  void Add_To_All(double Amount) {
    this.Adjust_Sum_All(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum_All(double Amount) {// raise or lower 'water level' of medium
    this.Amp_Prev += Amount;// does this help or hurt? 
    this.Adjust_Sum(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    this.Amp += Amount;
    this.Amp_Next += Amount;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  @Override public void ConnectCross(CellBase other, int Dim, int Axis) {
    this.NbrCross[Dim][Axis] = other;
    Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
    other.NbrCross[Dim][Axis] = this;// connect along same dimension, but opposite axis (eg your south connects to my north).
  }
  /* ********************************************************************************* */
  @Override public void DisconnectCross(int Dim, int Axis) {
    CellBase other = this.NbrCross[Dim][Axis];
    if (other != null) {
      this.NbrCross[Dim][Axis] = null;
      Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
      other.NbrCross[Dim][Axis] = null;// disconnect along same dimension, but opposite axis (eg your south disconnect from my north).
    }
  }
  /* ********************************************************************************* */
  @Override public void DisconnectAll() {
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        this.DisconnectCross(dcnt, axcnt);
      }
    }
  }
  /* ********************************************************************************* */
  @Override public void RandAmp() {
    this.Amp = 1.0 * (Globals.RandomGenerator.nextDouble() * 2.0 - 1.0);// range -1.0 to 1.0
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  @Override public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    String AmpTxt;
    double what = this.Amp;// / 8.0;// + 2;
    Color col = Globals.ToNegPos(what);
//    if (this.IsEmitter) {
//      col = Color.yellow;
//    }
    // draw cell background and outline
    ParentDC.gr.setColor(col);
    ParentDC.gr.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    ParentDC.gr.setColor(Color.black);
    ParentDC.gr.drawRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);

    if (false) { // draw mass fluid bar
      int Height = (int) (this.Fluid * this.Hgt);
      ParentDC.gr.setColor(Color.CYAN);
      ParentDC.gr.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) Height);
//      ParentDC.gr.setColor(Color.black);
//      ParentDC.gr.drawRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);

//      AmpTxt = String.format("%.2f", this.Fluid);
//      ParentDC.gr.setFont(new Font("TimesRoman", Font.PLAIN, 10));
//      ParentDC.gr.drawString(AmpTxt, (int) (this.XLoc), (int) (this.YLoc));
    }

    ParentDC.gr.setColor(Color.green);
    if (false) {
      AmpTxt = String.format("%.2f", this.Amp);
      ParentDC.gr.setFont(new Font("TimesRoman", Font.PLAIN, 10));
      ParentDC.gr.drawString(AmpTxt, (int) (this.XLoc), (int) (this.YLoc));
    }
    if (false) {
      ParentDC.gr.setFont(new Font("TimesRoman", Font.PLAIN, 10));

      AmpTxt = String.format("%.2f", this.AvgAmp);
      ParentDC.gr.drawString(AmpTxt, (int) (this.XLoc), (int) (this.YCtr + 4));
      if (false) {
        AmpTxt = String.format("%.2f", this.MaxAmp);
        ParentDC.gr.drawString(AmpTxt, (int) (this.XLoc), (int) (this.YLoc + 10));
        AmpTxt = String.format("%.2f", this.MinAmp);
        ParentDC.gr.drawString(AmpTxt, (int) (this.XLoc), (int) (this.YLoc + this.Hgt));
      }
    }
    if (false) {
      double hypot = this.PoyntingMag;
      double[] PVect = new double[NDims];
      if (Math.abs(hypot) > 0.00001) {// normalize length
        for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
//          PVect[DimCnt] = this.PoyntingVect[DimCnt] / hypot;// normalized 
          PVect[DimCnt] = this.PoyntingVect.Get(DimCnt) * 1.0;//5.0;
        }
        double len = this.Wdt / 2.0;
        ParentDC.gr.drawLine((int) (this.XCtr), (int) (this.YCtr), (int) (this.XCtr + PVect[0] * len), (int) (this.YCtr + PVect[1] * len));
      }
      ParentDC.gr.fillOval((int) (this.XCtr - 2), (int) (this.YCtr - 2), (int) (4), (int) (4));
    }
    if (this.IsWall) {
      ParentDC.gr.setColor(Color.green);
      ParentDC.gr.fillOval((int) (this.XCtr - 1), (int) (this.YCtr - 1), (int) (2), (int) (2));
    }
    if (false) {// vector grav line
      VectorGrav.Draw_Me(ParentDC);
      ParentDC.gr.setColor(Color.cyan);
      double Magnification = 10.0;//5.0;//10;// 100.0;// blow it up by arbitrary 100 so we can see it
      double Radius = this.StaticGrav * Magnification;// 100.0;// blow it up by arbitrary 100 so we can see it
      double Diameter = Radius * 2;
      ParentDC.gr.fillOval((int) (this.XCtr - Radius), (int) (this.YCtr - Radius), (int) (Diameter), (int) (Diameter));
    }
  }
  /* ********************************************************************************* */
  @Override public Cell Clone_Me() {
    Cell child = new Cell();
    return child;
  }
}
/*

 so the question is 

 // easy way
 previ = oew.az;
 nexti = oee.az;
 prevj = oen.az;
 nextj = oes.az;
 basis = (nexti + previ + nextj + prevj) * .25;// average of all neighbor's az
 a = oes.jx - oen.jx + oew.jy - oee.jy - (oe.az - basis);
 }
 o = oe.dazdt;
 oe.dazdt = (oe.dazdt * oe.damp) + a * forcecoef;
 oe.dazdt2 = oe.dazdt - o;
 */
