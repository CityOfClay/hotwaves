package HotWaves;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author MultiTool
 */
public class CellBase {
  public enum Type {
    NoType, CellBase, CellBt, CellWall
  };
  public Type MyType = Type.CellBase;
  public static final double TwoPi = Math.PI * 2.0;
  public static final int NDims = 2;
  public static final int NAxis = 2;
  public static final int NumNbrs = NDims * NAxis;
  public CellBase[][] NbrCross;// another way
  protected double TRate = 0.05;// time rate
//  protected double TRate = 0.0001;// time rate
  protected double TRate_Next;
  protected double TRateSquared;
  protected double Amp_Prev = 0.0, Amp = 0.0, Amp_Next = 0.0;
  protected double Tension = 0.0, Speed = 0.0, Speed_Next = 0.0;// Speed is also used as inertia
  protected double Damp = 1.0; //0.99;//0.5;//
  // weird stuff  
  public double Fluid = 0.0, Fluid_Next = 0.0;// fluid flow of grav field  
  public Vector VectorMass = null, VectorMass_Next = null, VectorGrav = null, VectorGrav_Next = null;
  protected double StaticGrav = 0.0, StaticGrav_Next = 0.0;
  private double MassOverDistance = 0.0;

  protected int XDex, YDex;
  public double XLoc = 0, YLoc = 0, Wdt = 10, Hgt = 10;// for drawing
  public double XCtr = 0.0, YCtr = 0.0;
  /* ********************************************************************************* */
  public void Init_NbrCross() {
    NbrCross = new CellBase[NDims][NAxis];
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        NbrCross[dcnt][axcnt] = null;
      }
    }
  }
  /* ********************************************************************************* */
  public void Add_MassOverDistance(double MassOverDist0) {
//    if (MassOverDist0 != 0.0) {
//      System.out.print(" ");
//    }
    this.MassOverDistance += MassOverDist0;
  }
  /* ********************************************************************************* */
  public void Set_Dex(int XDex0, int YDex0) {
    this.XDex = XDex0;
    this.YDex = YDex0;
    if (XDex0 != 0) {
      System.out.print("");
    }
  }
  /* ********************************************************************************* */
  public double Get_Amp() {
    return this.Amp;
  }
  /* ********************************************************************************* */
  public void Set_Amp(double Amp0) {
    this.Amp = Amp0;
//    double sqrT = Math.sqrt(this.TRate);// empirically this seems to work, but why?
//    this.Amp = Amp0 * sqrT;
  }
  /* ********************************************************************************* */
  public double Get_Speed() {
    return this.Speed;
  }
  /* ********************************************************************************* */
  public void Set_Damp(double Value) {
    this.Damp = Value;
  }
  /* ********************************************************************************* */
  public double Get_StaticGrav() {
    return this.StaticGrav;
  }
  /* ********************************************************************************* */
  public void SetCell(double Value) {// make cell emit a quantity
    // this looks like a desparate move, should probably delete
  }
  /* ********************************************************************************* */
  public void Assign_DrawBox(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {// location and size on screen
  }
  /* ********************************************************************************* */
  public void ConnectCross(CellBase other, int Dim, int Axis) {
    this.NbrCross[Dim][Axis] = other;
    Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
    other.NbrCross[Dim][Axis] = this;// connect along same dimension, but opposite axis (eg your south connects to my north).
  }
  /* ********************************************************************************* */
  public void DisconnectCross(int Dim, int Axis) {
    CellBase other = this.NbrCross[Dim][Axis];
    if (other != null) {
      this.NbrCross[Dim][Axis] = null;
      Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
      other.NbrCross[Dim][Axis] = null;// disconnect along same dimension, but opposite axis (eg your south disconnect from my north).
    }
  }
  /* ********************************************************************************* */
  public void DisconnectAll() {
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        this.DisconnectCross(dcnt, axcnt);
      }
    }
  }
  /* ********************************************************************************* */
  public void RandAmp() {
    this.Amp = 1.0 * (Globals.RandomGenerator.nextDouble() * 2.0 - 1.0);// range -1.0 to 1.0
  }
  /* ********************************************************************************* */
  public void Print_Me() {
    System.out.print("" + this.Amp + ", ");
  }
  /* ********************************************************************************* */
  public void Draw_Me(DrawingContext ParentDC) {// IDrawable
  }
  /* ********************************************************************************* */
  public void Update() {// getting close to runcycle
  }
  /* ********************************************************************************* */
  public void Rollover() {// set up for next cycle
  }
  /* ********************************************************************************* */
  public void Adjust_Sum_All(double Amount) {// raise or lower 'water level' of medium
  }
  /* ********************************************************************************* */
  public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    this.Amp += Amount;
    this.Amp_Next += Amount;
  }
  /* ********************************************************************************* */
  public CellBase Clone_Me() {
    CellBase child = new CellBase();
    return child;
  }
  /* ********************************************************************************* */
  void Set_TRate(double Value) {
    this.TRate = Value;
    this.TRateSquared = this.TRate * this.TRate;
    //System.out.println("TRate:" + this.TRate + ", TRateSquared:" + this.TRateSquared);
  }
  /* ********************************************************************************* */
  public double CalcTimeFactor() {// time dilation due to gravity
    double Rate = GetTimeFactor(this.MassOverDistance);
    this.Set_TRate(Rate);
    return Rate;
  }
  /* ********************************************************************************* */
  public static double GetTimeFactor(double MassOverDist) {// time dilation due to gravity
    double TimeFactor;// https://en.wikipedia.org/wiki/Gravitational_time_dilation
    double Extras = (2.0 * Globals.GravConst) / (Globals.CSqared);
    double ThingToRoot = 1.0 - (MassOverDist * Extras);
    if (ThingToRoot >= 0.0) {// Avoid square root of negative number.
      TimeFactor = Math.sqrt(ThingToRoot);
    } else {// Inside Schwarzschild radius just say time has stopped.
      TimeFactor = 0.0;
    }
    return TimeFactor;
  }
}
