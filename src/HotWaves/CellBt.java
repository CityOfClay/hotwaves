package HotWaves;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author MultiTool
 */
public class CellBt extends CellBase {// From bench test
//  public double Damp = 1.0; //0.99;//0.5;//
//  public double Tension = 0.0, Speed = 0.0, Speed_Next = 0.0;// Speed is also used as inertia
  public double Basis = 0.0;// average Amp of all my neighbors
//  private double Amp_Prev = 0.0, Amp = 0.0, Amp_Next = 0.0;
  private double MinAmp = Double.POSITIVE_INFINITY, MaxAmp = Double.NEGATIVE_INFINITY;
  double AvgAmp = 0.0, NumRuns = 0.0, SumAmp = 0;
  Vector PoyntingVect = null, PoyntingVectNormed = null;
  /* ********************************************************************************* */
  public CellBt() {
    MyType = Type.CellBt;
    Init_NbrCross();
    this.VectorMass = new Vector(NDims);
    this.VectorMass_Next = new Vector(NDims);
    this.VectorGrav = new Vector(NDims);
    this.VectorGrav_Next = new Vector(NDims);
    this.PoyntingVect = new Vector(NDims);
    this.PoyntingVectNormed = new Vector(NDims);
  }
  /* ********************************************************************************* */
  @Override public void SetCell(double Value) {// make cell emit a quantity
    CellBt cell = this;
    cell.Fluid = Value;
    cell.VectorMass.Fill(Value);
    cell.VectorMass_Next.Fill(Value);
    cell.VectorGrav.Fill(Value);
    cell.VectorGrav_Next.Fill(Value);
    cell.StaticGrav = 0.0;//Math.abs(Value);
    cell.StaticGrav_Next = 0.0;//Math.abs(Value);
  }
  /* ********************************************************************************* */
  @Override public void Assign_DrawBox(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {
    this.XLoc = XLoc0;
    this.YLoc = YLoc0;
    this.Wdt = Wdt0;
    this.Hgt = Hgt0;
    this.XCtr = this.XLoc + (this.Wdt / 2.0);
    this.YCtr = this.YLoc + (this.Hgt / 2.0);
    VectorMass.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorMass_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
  }
  /* ********************************************************************************* */
  public double Calc_Energy_Component(int Dim) {
    /* 
     If I understand correctly, Falstad seems to calculate the direction of energy flow (Poynting vector) by simply getting the difference 
     between the speeds of two opposite neighbors, and then multiplying by the current amp (height value) of the cell.
     By 'speed' I mean the rate of change of the altitude of the medium in each cell. 
     */
    double Component = 0.0;
    CellBase[] nbrs = this.NbrCross[Dim];
    CellBase nbr0 = nbrs[0];// NAxis
    CellBase nbr1 = nbrs[1];
    if (nbr0 != null && nbr1 != null) {// if energy ls flowing from nbr0 to nbr1, return value will be positive
      Component = nbr1.Speed - nbr0.Speed;// at this point the value is proportionate for any wavelength, but not the correct absolute magnitude. The vector will point in the right direction but with the wrong length.
    }
    return Component;
  }
  /* ********************************************************************************* */
  public void Calc_Energy_Vector(Vector PoyntingVect) {
    /*
    to do: Calc_Energy_Component to get E vector, then normalize E vector with pythag.
    then get energy of cell as abs(delta height (speed?)) and multiply that by E vector. 
     */
    double SumSq = 0.0;
    double Mag;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      Mag = this.Calc_Energy_Component(DimCnt);
      PoyntingVect.Set(DimCnt, Mag);
      SumSq += Mag * Mag;
    }
    this.PoyntingVectNormed.Copy_From(PoyntingVect);
    this.PoyntingVectNormed.Normalize();
//    this.PoyntingMag = Math.sqrt(SumSq);// pythagorean
//    double hypot = this.PoyntingMag;
//    if (Math.abs(hypot) < Globals.Fudge) {
//      hypot = Globals.Fudge * Math.signum(hypot);
//    }
//    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {// normalize length
//      this.PoyntingVectNormed.Set(DimCnt, PoyntingVect.Get(DimCnt) / hypot);// normalized 
//    }
//    double MyAmp = this.Get_Amp();
    double Energy = Math.abs(this.Amp - this.Amp_Prev);
    if (Math.abs(Energy) < Globals.Fudge) {
      Energy = Globals.Fudge * Math.signum(Energy);
    }
//    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {// amplify length
//      this.PoyntingVect[DimCnt] = this.PoyntingVectNormed[DimCnt] * Energy;// tied to change 
//    }
  }
  /* ********************************************************************************* */
  @Override public double Get_Amp() {
    return this.Amp;
  }
  /* ********************************************************************************* */
  @Override public void Set_Amp(double Amp0) {
    this.Amp = Amp0;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  public void Check_Amp() {
    if (this.MaxAmp < this.Amp) {
      this.MaxAmp = this.Amp;
    }
    if (this.MinAmp > this.Amp) {
      this.MinAmp = this.Amp;
    }
  }
  /* ********************************************************************************* */
  public double Calc_Basis(double DefaultBasis) {// Basis is just average of all neighbors. My amp is attracted to this value.
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    double Result = DefaultBasis;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.Get_Amp();
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      Result = Sum / NNbrs;// 4 neighbors in 2 dimensions
    }
    return Result;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Fluid() {// get average fluid height for diffusion test
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.Fluid;
          NNbrs++;
        }
      }
    }
    double AvgFluid = 0;
    if (NNbrs > 0) {
      AvgFluid = Sum / NNbrs;// 4 neighbors in 2 dimensions
    }
    return AvgFluid;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Tension(double Base) {
    return Base - this.Amp;// pull toward avg amp of neighbors
  }
  /* ********************************************************************************* */
  public double Calc_Speed() {
//    return (this.Amp - this.Prev_Amp)/this.TRate;// vector toward future state
    return (this.Amp - this.Amp_Prev) / 1.0;// vector toward future state
  }
  /* ********************************************************************************* */
  public void Calc_Amp(double OtherAmp) {// Calc_Amp and Rollover from bench testing.  this works.
    double Delta = OtherAmp - this.Amp;// Spring force
    Delta *= TRateSquared;
    this.Amp_Next = this.Amp + this.Speed;
    this.Amp_Next = this.Amp_Next + Delta;
    this.Speed_Next = this.Amp_Next - this.Amp;
    this.Speed_Next *= this.Damp;
  }
  /* ********************************************************************************* */
  @Override public void Update() {// getting close to runcycle
    // look for better alg:
    // https://www.gamedev.net/resources/_/technical/graphics-programming-and-theory/the-water-effect-explained-r915
    this.Basis = this.Calc_Basis(this.Basis);
    this.Tension = this.Calc_Tension(this.Basis);
    double GravLeakRate = 0.9999, GravDrag = 1.0 - GravLeakRate;
    this.Fluid_Next = (this.Fluid * GravDrag) + (this.Calc_Fluid() * GravLeakRate);// gravity flow
    /*
    So next amp is
    this amp plus its current speed, damped.
    plus the tension toward the average of neighbors, limited by timerate.
     */

    this.Calc_Amp(this.Basis);// Bench tested

    this.Calc_Energy_Vector(this.PoyntingVect);

    // statistics
    this.SumAmp += this.Amp;
    this.NumRuns++;
    this.AvgAmp = this.SumAmp / this.NumRuns;
  }
  /* ********************************************************************************* */
  @Override public void Rollover() {// set up for next cycle
    this.Amp_Prev = this.Amp;// Bench tested
    this.Amp = this.Amp_Next;
    this.Speed = this.Speed_Next;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  void Add_To_All(double Amount) {
    this.Adjust_Sum_All(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum_All(double Amount) {// raise or lower 'water level' of medium
    this.Amp_Prev += Amount;// does this help or hurt? 
    this.Adjust_Sum(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    this.Amp += Amount;
    this.Amp_Next += Amount;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
//  @Override public void ConnectCross(CellBase other, int Dim, int Axis) {
//    this.NbrCross[Dim][Axis] = other;
//    Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
//    other.NbrCross[Dim][Axis] = this;// connect along same dimension, but opposite axis (eg your south connects to my north).
//  }
  /* ********************************************************************************* */
  @Override public void DisconnectCross(int Dim, int Axis) {
    CellBase other = this.NbrCross[Dim][Axis];
    if (other != null) {
      this.NbrCross[Dim][Axis] = null;
      Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
      other.NbrCross[Dim][Axis] = null;// disconnect along same dimension, but opposite axis (eg your south disconnect from my north).
    }
  }
  /* ********************************************************************************* */
  @Override public void DisconnectAll() {
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        this.DisconnectCross(dcnt, axcnt);
      }
    }
  }
  /* ********************************************************************************* */
  @Override public void RandAmp() {
    this.Amp = 1.0 * (Globals.RandomGenerator.nextDouble() * 2.0 - 1.0);// range -1.0 to 1.0
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  @Override public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    double what = this.Amp * 40;// / 8.0;// + 2;
    Color col = Globals.ToNegPos(what);
    // draw cell background and outline
    ParentDC.gr.setColor(col);
    ParentDC.gr.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    if (false) { // outline
      ParentDC.gr.setColor(Color.black);
      ParentDC.gr.drawRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    }
    //ParentDC.gr.setColor(Color.green);
  }
  /* ********************************************************************************* */
  @Override public CellBt Clone_Me() {
    CellBt child = new CellBt();
    return child;
  }
}
