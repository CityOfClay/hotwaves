package HotWaves;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author MultiTool
 */
public class CellOsc extends CellBase {
//  public double Damp = 1.0; //0.99;//0.5;//
//  public double Tension = 0.0, Speed = 0.0, Speed_Next = 0.0;// Speed is also used as inertia
  public double Basis = 0.0;// average Amp of all my neighbors
//  private double Amp_Prev = 0.0, Amp = 0.0, Amp_Next = 0.0;
  private double MinAmp = Double.POSITIVE_INFINITY, MaxAmp = Double.NEGATIVE_INFINITY;
  double AvgAmp = 0.0, NumRuns = 0.0, SumAmp = 0;
  Vector PoyntingVect = null, PoyntingVectNormed = null;
  private double Cycle = 0.0;
  /* ********************************************************************************* */
  public CellOsc() {
    MyType = Type.CellBt;
    Init_NbrCross();
    this.VectorMass = new Vector(NDims);
    this.VectorMass_Next = new Vector(NDims);
    this.VectorGrav = new Vector(NDims);
    this.VectorGrav_Next = new Vector(NDims);
    this.PoyntingVect = new Vector(NDims);
    this.PoyntingVectNormed = new Vector(NDims);
  }
  /* ********************************************************************************* */
  @Override public double Get_Amp() {
    return this.Amp;
  }
  /* ********************************************************************************* */
  @Override public void Set_Amp(double Amp0) {
    this.Amp = Amp0;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  public void Check_Amp() {
    if (this.MaxAmp < this.Amp) {
      this.MaxAmp = this.Amp;
    }
    if (this.MinAmp > this.Amp) {
      this.MinAmp = this.Amp;
    }
  }
  /* ********************************************************************************* */
  @Override public void Assign_DrawBox(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {
    this.XLoc = XLoc0;
    this.YLoc = YLoc0;
    this.Wdt = Wdt0;
    this.Hgt = Hgt0;
    this.XCtr = this.XLoc + (this.Wdt / 2.0);
    this.YCtr = this.YLoc + (this.Hgt / 2.0);
    VectorMass.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorMass_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
    VectorGrav_Next.AssignDrawingProperties(this.XLoc, this.YLoc, this.Wdt, this.Hgt);
  }
  /* ********************************************************************************* */
  public double Calc_Basis(double DefaultBasis) {// Basis is just average of all neighbors. My amp is attracted to this value.
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    double Result = DefaultBasis;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          Sum += nbr.Get_Amp();
          NNbrs++;
        }
      }
    }
    if (NNbrs > 0) {
      Result = Sum / NNbrs;// 4 neighbors in 2 dimensions
    }
    return Result;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Tension(double Base) {
    return Base - this.Amp;// pull toward avg amp of neighbors
  }
  /* ********************************************************************************* */
  public double Calc_Speed() {
    return (this.Amp - this.Amp_Prev) / 1.0;// vector toward future state
  }
  public double NumCycles = 0;
  public double Increment = 0.01;// 0.02;
  /* ********************************************************************************* */
  @Override public void Update() {// getting close to runcycle
    // look for better alg:
    this.Basis = this.Calc_Basis(this.Basis);
    this.Tension = this.Calc_Tension(this.Basis);
    {
      double MagFactor = 5.0;//1.0;//10.0;//0.5;//
      MagFactor = 0.25;
      MagFactor = 1.0;
      this.Amp_Next = MagFactor * Math.sin(this.Cycle * TwoPi);
//      this.Amp_Next = MagFactor;// continuous faucet, no oscillation
//      this.Cycle += 0.01;
//      this.Cycle += 0.005;
//      this.Cycle += 0.025;
//      this.Cycle += 0.03;
//      this.Cycle += 0.02;// good for fiber optic
      if (this.NumRuns < 3400) {
      this.Cycle += Increment;
      }
//      this.Cycle += 0.06;// good for timefield
//      this.Cycle += 0.08;
      this.Speed_Next = this.Amp_Next - this.Amp;
      this.Speed_Next *= this.Damp;
    }
    // statistics
    this.NumCycles = this.NumRuns * Increment;
    this.SumAmp += this.Amp;
    this.NumRuns++;
    this.AvgAmp = this.SumAmp / this.NumRuns;
  }
  /* ********************************************************************************* */
  @Override public void Rollover() {// set up for next cycle
    this.Amp_Prev = this.Amp;// Bench tested
    this.Amp = this.Amp_Next;
    this.Speed = this.Speed_Next;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  void Add_To_All(double Amount) {
    this.Adjust_Sum_All(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum_All(double Amount) {// raise or lower 'water level' of medium
    this.Amp_Prev += Amount;// does this help or hurt? 
    this.Adjust_Sum(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
  }
  /* ********************************************************************************* */
  @Override public void DisconnectAll() {
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        this.DisconnectCross(dcnt, axcnt);
      }
    }
  }
  /* ********************************************************************************* */
  @Override public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    double what = this.Amp * 40;// / 8.0;// + 2;
    Color col = Globals.ToNegPos(what);
    // draw cell background and outline
    ParentDC.gr.setColor(col);
    ParentDC.gr.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    ParentDC.gr.setColor(Color.black);
    ParentDC.gr.drawRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    ParentDC.gr.setColor(Color.green);
    //System.out.println("Amp:" + this.Amp);
  }
  /* ********************************************************************************* */
  @Override public CellOsc Clone_Me() {
    CellOsc child = new CellOsc();
    return child;
  }
}
