package HotWaves;

import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */

/* ********************************************************************************* */
public class CellRow {//extends ArrayList<CellBase> {
  CellBase[] cells;
  private int YDex = Integer.MIN_VALUE;// crash if uninitialized
  /* ********************************************************************************* */
  public CellRow(int width) {
    this.cells = new CellBase[width];
  }
  /* ********************************************************************************* */
  public CellRow(CellBase seed, int width) {
    this.cells = new CellBase[width];
    for (int cnt = 0; cnt < width; cnt++) {
      CellBase cell = seed.Clone_Me();// new CellBase();
      this.cells[cnt] = cell;
    }
  }
  /* ********************************************************************************* */
  public void Set_Dex(int YDex0) {
    this.YDex = YDex0;
  }
  /* ********************************************************************************* */
  public void FillWithCells(CellBase seed, int StartDex, int EndDex) {
    for (int cnt = StartDex; cnt < EndDex; cnt++) {// not inclusive?
      CellBase cell = seed.Clone_Me();
      this.cells[cnt] = cell;
      cell.Set_Dex(cnt, this.YDex);
    }
  }
  /* ********************************************************************************* */
  public CellBase Get(int Dex) {
    return this.cells[Dex];
  }
  /* ********************************************************************************* */
  public int size() {
    return this.cells.length;
  }
  /* ********************************************************************************* */
  public void Assign_DrawBox(double XLoc, double YLoc, double Width, double Height) {
    int NumCells = this.cells.length;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      CellBase cell = this.cells[cnt];// location and size on screen
      cell.Assign_DrawBox(XLoc, YLoc, Width, Height);
      XLoc += Width;
    }
  }
  /* ********************************************************************************* */
  public void ConnectRows(CellRow otherprev) {
    int NumCells = this.cells.length;
    if (NumCells != otherprev.cells.length) {
      System.out.println("Row sizes do not match!!!");// would be throw but I hate catch. 
    }
    for (int cnt = 0; cnt < NumCells; cnt++) {
      CellBase cell0 = otherprev.cells[cnt];
      CellBase cell1 = this.cells[cnt];
      cell1.ConnectCross(cell0, 1, 0);// 1 is ydim, 0 is north axis
    }
  }
  /* ********************************************************************************* */
  public void ConnectCells() {// wrapped horizontal connections
    int NumCells = this.cells.length;
    CellBase cell_prev = null, cell_now = this.Get(NumCells - 1);
    for (int XCnt = 0; XCnt < NumCells; XCnt++) {
      cell_prev = cell_now;
      cell_now = this.Get(XCnt);
      cell_now.ConnectCross(cell_prev, 0, 0);// 0 is xdim, 0 is west axis
    }
  }
  /* ********************************************************************************* */
  public void Fill_TimeRate(double Value) {
    int NumCells = this.cells.length;
    Fill_TimeRate(0, NumCells, Value);
  }
  /* ********************************************************************************* */
  public void Fill_TimeRate(int FromX, int ToX, double Value) {
    int NumCells = this.cells.length;
    FromX = Math.min(NumCells, FromX);
    ToX = Math.min(NumCells, ToX);
    CellBase cell;
    for (int cnt = FromX; cnt < ToX; cnt++) {
      cell = this.Get(cnt);
      cell.Set_TRate(Value);
    }
  }
  /* ********************************************************************************* */
  public void Fill_Damps(int FromX, int ToX, double Value) {
    int NumCells = this.cells.length;
    FromX = Math.min(NumCells, FromX);
    ToX = Math.min(NumCells, ToX);
    CellBase cell;// FromX is inclusive, ToX is exclusive
    for (int cnt = FromX; cnt < ToX; cnt++) {
      cell = this.Get(cnt);
      cell.Set_Damp(Value);
    }
  }
  /* ********************************************************************************* */
  public void Fill_Amps(double Value) {
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Set_Amp(Value);
    }
  }
  /* ********************************************************************************* */
  public void Fill_Grav(double Value) {
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Fluid = Value;
      cell.SetCell(Value);
    }
  }
  /* ********************************************************************************* */
  public void Fill_Amps(int FromX, int ToX, double Value) {
    FromX = Math.min(this.cells.length, FromX);
    ToX = Math.min(this.cells.length, ToX);
    CellBase cell;
    for (int cnt = FromX; cnt < ToX; cnt++) {
      cell = this.Get(cnt);
      cell.Set_Amp(Value);
    }
  }
  /* ********************************************************************************* */
  public void Rand_Amps() {
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.RandAmp();
    }
  }
  /* ********************************************************************************* */
  public void Update() {// runcycle
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Update();
    }
  }
  /* ********************************************************************************* */
  public void Rollover() {// set up for next cycle
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Rollover();
    }
  }
  /* ********************************************************************************* */
  public double Get_Sum() {// get sum of all cell Amps
    int NumCells = this.cells.length;
    CellBase cell;
    double Sum = 0.0;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      Sum += cell.Get_Amp();
    }
    return Sum;
  }
  /* ********************************************************************************* */
  public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    int NumCells = this.cells.length;
    CellBase cell;
    Amount /= (double) NumCells;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Adjust_Sum(Amount);
    }
  }
  /* ********************************************************************************* */
  public void CalcTimeFactor() {// time dilation due to gravity
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.CalcTimeFactor();
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Draw_Me(ParentDC);
    }
  }
  /* ********************************************************************************* */
  public void Print_Me() {
    int NumCells = this.cells.length;
    CellBase cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Get(cnt);
      cell.Print_Me();
    }
    System.out.println();
  }
}
