package HotWaves;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author MultiTool
 */
public class CellWall extends CellBase {
  public double Basis = 0.0;// average Amp of all my neighbors
  private double MinAmp = Double.POSITIVE_INFINITY, MaxAmp = Double.NEGATIVE_INFINITY;
  double AvgAmp = 0.0, NumRuns = 0.0, SumAmp = 0;
  /* ********************************************************************************* */
  public CellWall() {
    MyType = Type.CellWall;
    Init_NbrCross();
  }
  /* ********************************************************************************* */
  @Override public void SetCell(double Value) {// make cell emit a quantity
    CellWall cell = this;
  }
  /* ********************************************************************************* */
  @Override public void Assign_DrawBox(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {
    this.XLoc = XLoc0;
    this.YLoc = YLoc0;
    this.Wdt = Wdt0;
    this.Hgt = Hgt0;
    this.XCtr = this.XLoc + (this.Wdt / 2.0);
    this.YCtr = this.YLoc + (this.Hgt / 2.0);
  }
  /* ********************************************************************************* */
  @Override public double Get_Amp() {
    return this.Amp;
  }
  /* ********************************************************************************* */
  @Override public void Set_Amp(double Amp0) {
    this.Amp = Amp0;
//    double sqrT = Math.sqrt(this.TRate);// empirically this seems to work, but why?
//    this.Amp = Amp0 * sqrT;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  public void Check_Amp() {
    if (this.MaxAmp < this.Amp) {
      this.MaxAmp = this.Amp;
    }
    if (this.MinAmp > this.Amp) {
      this.MinAmp = this.Amp;
    }
  }
  /* ********************************************************************************* */
  public double Calc_Basis(double DefaultBasis) {// Basis is just average of all neighbors. My amp is attracted to this value.
    double Sum = 0;
    double NNbrs = 0;
    CellBase nbr;
    double Result = DefaultBasis;
    for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
      for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
        nbr = this.NbrCross[DimCnt][AxCnt];
        if (nbr != null) {
          if (false) {
            if (nbr.MyType != CellBase.Type.CellWall) {// hacky, only see non-walls
              Sum += nbr.Get_Amp();
              NNbrs++;
            }
          } else {
            Sum += nbr.Get_Amp();
            NNbrs++;
          }
        }
      }
    }
    if (NNbrs > 0) {
      //Sum += this.Amp; NNbrs++;
      Result = Sum / NNbrs;// 4 neighbors in 2 dimensions
    } else {
      Result = DefaultBasis;
    }
    return Result;// 4 neighbors in 2 dimensions
  }
  /* ********************************************************************************* */
  public double Calc_Tension(double Base) {
    return Base - this.Amp;// pull toward avg amp of neighbors
  }
  /* ********************************************************************************* */
  public double Calc_Speed() {
//    return (this.Amp - this.Prev_Amp)/this.TRate;// vector toward future state
    return (this.Amp - this.Amp_Prev) / 1.0;// vector toward future state
  }
  /* ********************************************************************************* */
  public void Calc_Amp(double OtherAmp) {// Calc_Amp and Rollover from bench testing.  this works.
    if (true) {
      double Delta = OtherAmp - this.Amp;// Spring force
      Delta *= TRateSquared;
      this.Amp_Next = this.Amp + this.Speed;
      this.Amp_Next = this.Amp_Next + Delta;
      this.Speed_Next = this.Amp_Next - this.Amp;
      this.Speed_Next *= this.Damp;
    } else {// hacky
      this.Amp_Next = OtherAmp;// just directly copy my neighbor(s)
    }
  }
  /* ********************************************************************************* */
  @Override public void Update() {// getting close to runcycle
    // look for better alg:
    // https://www.gamedev.net/resources/_/technical/graphics-programming-and-theory/the-water-effect-explained-r915
    
    // this.TRate = 1.0; this.Speed = 0.0;
    this.Basis = this.Calc_Basis(this.Basis);
    this.Tension = this.Calc_Tension(this.Basis);
    /*
     So next amp is
     this amp plus its current speed, damped.
     plus the tension toward the average of neighbors, limited by timerate.
     */

    this.Calc_Amp(this.Basis);

    // statistics
    this.SumAmp += this.Amp;
    this.NumRuns++;
    this.AvgAmp = this.SumAmp / this.NumRuns;
  }
  /* ********************************************************************************* */
  @Override public void Rollover() {// set up for next cycle
    this.Amp_Prev = this.Amp;// Bench tested
    this.Amp = this.Amp_Next;
    this.Speed = this.Speed_Next;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  void Add_To_All(double Amount) {
    this.Adjust_Sum_All(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum_All(double Amount) {// raise or lower 'water level' of medium
    this.Amp_Prev += Amount;// does this help or hurt? 
    this.Adjust_Sum(Amount);
  }
  /* ********************************************************************************* */
  @Override public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    this.Amp += Amount;
    this.Amp_Next += Amount;
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  @Override public void ConnectCross(CellBase other, int Dim, int Axis) {
    this.NbrCross[Dim][Axis] = other;
    Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
    other.NbrCross[Dim][Axis] = this;// connect along same dimension, but opposite axis (eg your south connects to my north).
  }
  /* ********************************************************************************* */
  @Override public void DisconnectCross(int Dim, int Axis) {
    CellBase other = this.NbrCross[Dim][Axis];
    if (other != null) {
      this.NbrCross[Dim][Axis] = null;
      Axis = (1 - Axis);// 1-1 = 0.  1-0 = 1.
      other.NbrCross[Dim][Axis] = null;// disconnect along same dimension, but opposite axis (eg your south disconnect from my north).
    }
  }
  /* ********************************************************************************* */
  @Override public void DisconnectAll() {
    for (int dcnt = 0; dcnt < NDims; dcnt++) {
      for (int axcnt = 0; axcnt < NAxis; axcnt++) {
        this.DisconnectCross(dcnt, axcnt);
      }
    }
  }
  /* ********************************************************************************* */
  @Override public void RandAmp() {
    this.Amp = 1.0 * (Globals.RandomGenerator.nextDouble() * 2.0 - 1.0);// range -1.0 to 1.0
    this.Check_Amp();
  }
  /* ********************************************************************************* */
  @Override public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    double what = this.Amp * 40;// / 8.0;// + 2;
    Color col = Globals.ToNegPos(what);
    // draw cell background and outline
    ParentDC.gr.setColor(col);
    ParentDC.gr.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    ParentDC.gr.setColor(Color.black);
    ParentDC.gr.drawRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
    ParentDC.gr.setColor(Color.green);
  }
  /* ********************************************************************************* */
  @Override public CellWall Clone_Me() {
    CellWall child = new CellWall();
    return child;
  }
}
