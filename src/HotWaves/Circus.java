/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HotWaves;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.imageio.ImageIO;

/**
 *
 * @author
 */
public class Circus {
  public static int NDims = 4;//3;//4;
  /* ******************************************************************************* */
  public static String ObjPnt(double XLoc, double YLoc, double ZLoc) {
    //String txtln = String.format("v %.8f %.8f %.8f", XLoc, YLoc, ZLoc);
    //String txtln = String.format("v %.16f %.16f %.16f", XLoc, ZLoc, YLoc);
    String txtln = String.format("v %.16f %.16f %.16f", XLoc, YLoc, ZLoc);
    return txtln;
  }
  /* ******************************************************************************* */
  public static class Diver {// explores recurrent function surface
    public double[] Base = new double[NDims];
    /* ******************************************************************************* */
    public Diver() {
      for (int cnt = 0; cnt < NDims; cnt++) {
        Base[cnt] = 0.0;
      }
    }
    /* ******************************************************************************* */
    public void SetBase2D(double Dim0, double Dim1) {
      this.Base[0] = Dim0;
      this.Base[1] = Dim1;
    }
    /* ******************************************************************************* */
    public void SetBase3D(double Dim0, double Dim1, double Dim2) {
      this.Base[0] = Dim0;
      this.Base[1] = Dim1;
      this.Base[2] = Dim2;
    }
    /* ******************************************************************************* */
    public void SetBase4D(double Dim0, double Dim1, double Dim2, double Dim3) {
      this.Base[0] = Dim0;
      this.Base[1] = Dim1;
      this.Base[2] = Dim2;
      this.Base[3] = Dim3;
    }
    /* ******************************************************************************* */
    public void SetBaseND(double[] Vector) {
      this.Base = Vector.clone();
      //System.arraycopy(Vector, 0, this.Base, 0, NDims);
    }
    /* ******************************************************************************* */
    public double GetHeight(double Distance) {
      double Sum = 0;
      for (int cnt = 0; cnt < NDims; cnt++) {
        Sum += 1.0 / (this.Base[cnt] + Distance);
      }
      return Sum;
    }
    /* ******************************************************************************* */
    public double GetSlope(double Distance0, double Distance1) {
      double Height0, Height1;
      Height0 = this.GetHeight(Distance0);
      Height1 = this.GetHeight(Distance1);
      double Slope = (Height1 - Height0) / (Distance0 - Distance1);
      return Slope;
    }
    /* ******************************************************************************* */
    public double Tree_Search_Distance(double TargetHeight, double MinDist, double MaxDist) {
      double MedDist;
      double SampleHeight = Double.NEGATIVE_INFINITY;
      int SearchDepth = 1 << 16;// 65536 // 128;// 64;//10000000;// 
      int DepthCnt;
      for (DepthCnt = 0; DepthCnt < SearchDepth; DepthCnt++) {
        MedDist = (MinDist + MaxDist) / 2.0;
//        SampleHeight = MedDist;
        SampleHeight = this.GetHeight(MedDist);// GetHeight is in descending order of distance.
        //System.out.println("" + DepthCnt + ", " + MedDist + ", " + SampleHeight + ", ");
        //if (MedDist == MaxDist) { System.out.println(""); }
        if (TargetHeight > SampleHeight) {
          MaxDist = MedDist;
        } else if (TargetHeight < SampleHeight) {
          MinDist = MedDist;
        } else {
          MinDist = MedDist;// does equal ever happen?
        }
      }
      if (false) {
        if (TargetHeight != SampleHeight) {
          System.out.println("Inequality! T:" + TargetHeight + ", S:" + SampleHeight + ", ");
        }
      }
      return MinDist;
    }
    /* ********************************************************************** */
    public void TestFitter(double Dist0, double Dist1, double Dist2, double Dist3, Fitter fitter) {
      this.SetBase4D(Dist0, Dist1, Dist2, Dist3);
      double DistStep = 1.0;
      double Reach0 = 0, Reach1 = Reach0 + DistStep;
      double Height0, Height1;
      Height0 = this.GetHeight(Reach0);
      Height1 = this.GetHeight(Reach1);
      fitter.Recur(Height0, Height1);

      // next how do we trace the diver?
      System.out.println("Diver");
      double HeightPrev, HeightNow, HeightNext;
      double DistancePrev, DistanceNow, DistanceNext;
      DistancePrev = 0.0;// first 2 heights are xy coords, third is color
      DistanceNow = DistStep;
      DistanceNext = DistanceNow + DistStep;
      double NumSteps = 1000;//100;
      while (DistanceNext < NumSteps) {
        HeightPrev = this.GetHeight(DistancePrev);
        HeightNow = this.GetHeight(DistanceNow);
        HeightNext = this.GetHeight(DistanceNext);
        System.out.println("" + HeightPrev + ", " + HeightNow + ", " + HeightNext + ", ");
        DistancePrev = DistanceNow;
        DistanceNow = DistanceNext;
        DistanceNext += DistStep;
      }
      System.out.println("Done");
    }
    /* ********************************************************************** */
    public void Recur(double Seed0, double Seed1) {
      /*
       so given starting coordinates, trace a plot forward.
       each coordinate says how far away is a different center of mass.
      
       start with 4 grav loc coordinates
       get first 2 consecutive heights, distance step=1.0
       feed those heights as seeds to fitter and then feed back, and plot
      
       */
      double Step = 1.0;
      double PlotLength = 100;
      double HeightPrev, HeightNow, HeightNext;
      double DistancePrev, DistanceNow, DistanceNext;
      DistancePrev = 0.0;// first 2 heights are xy coords, third is color
      DistanceNow = Step;
      DistanceNext = DistanceNow + Step;
      while (DistanceNext < PlotLength) {
        HeightPrev = this.GetHeight(DistancePrev);
        HeightNow = this.GetHeight(DistanceNow);
        HeightNext = this.GetHeight(DistanceNext);
        System.out.println("" + HeightPrev + ", " + HeightNow + ", " + HeightNext + ", ");
        DistancePrev = DistanceNow;
        DistanceNow = DistanceNext;
        DistanceNext += Step;
      }
    }
    /* ******************************************************************************* */
    public double FindNextHeight(double XLoc, double YLoc) {
      double TreeSearchMax = 1000;
      double ResultDistanceX = this.Tree_Search_Distance(XLoc, 0.0, TreeSearchMax);
      double ResultDistanceY = this.Tree_Search_Distance(YLoc, 0.0, TreeSearchMax);
      double DistStep = ResultDistanceY - ResultDistanceX;
      double DistanceNext = ResultDistanceY + DistStep;
      double ZLoc = this.GetHeight(DistanceNext);
      return ZLoc;
    }
  }
  /* ******************************************************************************* */
  public static class Record {
    public double Height, Distance, Slope, Height_Down_Hill;
    public double[] Loc = new double[NDims];
    public void Print_Me() {
      //System.out.println("" + this.Height + ", " + this.Height_Down_Hill + ", " + this.Distance + ", " + this.Slope + ", ");
      System.out.println("" + this.Height + ", " + this.Height_Down_Hill + ", " + this.Slope + ", ");
    }
  }
  /* ******************************************************************************* */
  public static String PlotTest() {
    StringBuilder sb = new StringBuilder();
    double size = 4.0;
    double step = 0.01;
    double halfsize = size / 2.0;
    String NumeratorTxt = String.format("%.2f", step), DenomTxt;
    int TestCounter = 0;
    for (double cnt = 0; cnt < size; cnt += step) {// for http://fooplot.com
      DenomTxt = String.format("%.2f", cnt - halfsize);
      sb.append("abs(" + NumeratorTxt + "/(x+" + DenomTxt + ")) + ");
      TestCounter++;
    }
    String txt = sb.toString();
    System.out.println(txt);
    return txt;
  }
  /* ******************************************************************************* */
  public static void Test2() {
    Diver diver = new Diver();
    double TargetHeight = 0.71;// 100000.67;
    double ResultDistance;
    if (false) {
      System.out.println("TargetHeight:" + TargetHeight + "");
      diver.SetBase3D(0, 0, 0);
      diver.SetBaseND(new double[]{0, 0, 0});
      ResultDistance = diver.Tree_Search_Distance(TargetHeight, 0.0, 1.0);
      System.out.println("Result:" + ResultDistance + "");// 2.9999799001346648E-5, 100000.67000000014,
    }
    /*
     march diver base across first 2 dimensions, 0 in third
     then treesearch to a given iso height for every square in march grid
     get the uphill and downhill slopes at that point.  Maybe far downhill slopes?
     save all the heights in a collection (along with their slopes, distances and bases)
    
     sort the collection by slope.
     look for any pair of things that have the same slope, but different downhill slopes.  different outcome traces. 
    
     */
    //double Step = 0.1, Limit = 1.0;
    double Step = 1.0, BoxScanWidth = 10.0, TreeSearchMax = 100;
    //Step = 0.1;
    double Slope, SlopeDelta = 0.00000001;
    SlopeDelta = 0.0001;
    double Distance_Down_Hill = 0.5;// far downslope
    double ZLoc = 0;
    ArrayList<Record> recs = new ArrayList<Record>();
    for (double XLoc = 0.0; XLoc < BoxScanWidth; XLoc += Step) {
      for (double YLoc = 0.0; YLoc < BoxScanWidth; YLoc += Step) {
//      diver.SetBase2D(XLoc, 0);
        diver.SetBase3D(XLoc, YLoc, ZLoc);
        ResultDistance = diver.Tree_Search_Distance(TargetHeight, 0.0, TreeSearchMax);
        Slope = diver.GetSlope(ResultDistance, ResultDistance + SlopeDelta);
        Record rec = new Record();
        rec.Loc[0] = XLoc;
        rec.Loc[1] = YLoc;
        rec.Loc[2] = ZLoc;
        //rec.Height = TargetHeight;
        rec.Height = diver.GetHeight(ResultDistance);
        rec.Height_Down_Hill = diver.GetHeight(ResultDistance + Distance_Down_Hill);
        rec.Distance = ResultDistance;
        rec.Slope = Slope;
        recs.add(rec);
      }
    }

    Sort_By_Slope(recs);
    //Sort_By_Height(recs);

    double Prev_Distance = -100, Prev_Slope = -100, Prev_Slope_Down_Hill = -100, Slope_Down_Hill = -100;
    double Prev_Height = -100, Height, Prev_Height_Down_Hill = -100, Height_Down_Hill;
    SlopeDelta = 0.5;// far downslope
    for (int cnt = 0; cnt < recs.size(); cnt++) {
      Record rec = recs.get(cnt);

      rec.Print_Me();// Height, Height_Down_Hill, Distance, Slope 

      Height_Down_Hill = diver.GetHeight(rec.Distance + SlopeDelta);
      Slope_Down_Hill = diver.GetSlope(rec.Distance, rec.Distance + SlopeDelta);
      if (true) {
        /*
         We are looking for 2 identical slopes in a row (all heights are the same),
         then see if downhill they are followed by different slopes or heights
         */
        if (Similar(rec.Slope, Prev_Slope)) {// this check only works if Sort_By_Slope has been done
          // looking for 2 lines that have the same height and slope, but turn out differently.
          if (!Similar(Prev_Slope_Down_Hill, Slope_Down_Hill)) {
            System.out.println("Problem! " + Prev_Slope_Down_Hill + ", " + Slope_Down_Hill + ", ");
          }
          if (!Similar(Prev_Height_Down_Hill, Height_Down_Hill)) {
            System.out.println("Problem! " + Prev_Height_Down_Hill + ", " + Height_Down_Hill + ", ");
          }
        }
      }
      Prev_Height = rec.Height;
      Prev_Distance = rec.Distance;
      Prev_Slope = rec.Slope;
      Prev_Height_Down_Hill = Height_Down_Hill;
      Prev_Slope_Down_Hill = Slope_Down_Hill;
    }

    System.out.println();//0.6699999999999999
  }
  /* ********************************************************************************* */
  public static boolean Similar(double Num0, double Num1) {
    double Diff = Num0 - Num1;
//    double Tolerance = 1.0e-20;
    double Tolerance = 1.0e-12;
    if (Math.abs(Diff) < Tolerance) {
      return true;
    } else {
      return false;
    }
  }
  /* ********************************************************************************* */
  public static void Sort_By_Slope(ArrayList<Record> recs) {
    Collections.sort(recs, new Comparator<Record>() {
      @Override public int compare(Record rec0, Record rec1) {
        return Double.compare(rec0.Slope, rec1.Slope);
      }
    });
  }
  /* ********************************************************************************* */
  public static void Sort_By_Height(ArrayList<Record> recs) {
    Collections.sort(recs, new Comparator<Record>() {
      @Override public int compare(Record rec0, Record rec1) {
        return Double.compare(rec0.Height, rec1.Height);
      }
    });
  }
  /* ********************************************************************************* */
  public static PrintWriter Open_File(String FileName) {
    PrintWriter out = null;
    try {
      out = new PrintWriter(FileName);
    } catch (Exception ex) {
      System.out.println(ex.getStackTrace());
    }
    return out;
  }
  /* ******************************************************************************* */
  public static void HeightGrid() {
    Diver diver = new Diver();
    Diver diver2 = new Diver();
    double DistStep, TreeSearchMax = 10000;
    double HgtStep = Math.PI, BoxScanWidth = 100.0, BoxScanHeight = 100.0;
    double ZLoc, ZLoc2, DistanceNext, ResultDistanceX, ResultDistanceY;
    String txtln;
    HgtStep = 1.0;

    //diver.SetBaseND(new double[]{0.9, 50.0, 53.0, 23.0});// FLAT?? not coincident
//    diver.SetBaseND(new double[]{0.0, 0.0, 53.0, 27.0});// coincident
//    diver.SetBaseND(new double[]{0.0, 50.0, 53.0, 27.0});// coincident
//    diver.SetBaseND(new double[]{0.9, 50.0, 53.0, 27.0});// not coincident
//    diver.SetBaseND(new double[]{0.9, 0.0, 0.0, 0.0});// coincident
    if (NDims == 3) {// crazy stupid if then
      diver.SetBaseND(new double[]{10.0, 10.0, 10.0});// not coincident
    } else if (NDims == 4) {
      diver.SetBaseND(new double[]{0.0, 0.0, 0.0, 0.0});// coincident
//      diver.SetBaseND(new double[]{10.0, 10.0, 10.0, 10.0});// not coincident
//      diver.SetBaseND(new double[]{10.0, 10.0, 10.0, 0.0});// coincident
//      double starter = 0.1;// 0.1 for everything gives weird results
//      diver.SetBaseND(new double[]{starter, starter, starter, starter});// 

      double starter = BoxScanWidth;// 0.1 for everything gives weird results
//      double starter = 0.0;
      diver.SetBaseND(new double[]{0.0, starter, starter, starter});// 
//      diver2.SetBaseND(new double[]{starter, 0.0, starter * 3.7, 0.0});// 
      diver2.SetBaseND(new double[]{0.0, 0.0, 0.0, 0.0});// 
    }

    PrintWriter out = Open_File("PntGrid.obj");
    out.println("o Grid");
    double Extreme = BoxScanWidth * 2;
    txtln = Circus.ObjPnt(Extreme, 0.0, 0.0);// marker
    out.println(txtln);
    for (double XLoc = 0.0; XLoc < BoxScanWidth; XLoc += HgtStep) {
      //System.out.println("X:" + XLoc + ", ");
      for (double YLoc = 0.0; YLoc < BoxScanHeight; YLoc += HgtStep) {
        ZLoc = diver.FindNextHeight(XLoc, YLoc);
        ZLoc2 = diver2.FindNextHeight(XLoc, YLoc);
        if (!Similar(ZLoc, ZLoc2)) {
          double Diff = ZLoc - ZLoc2;
//              txtln = Circus.ObjPnt(XLoc, YLoc, ZLoc);
//            txtln = Circus.ObjPnt(XLoc, YLoc, ZLoc2);
//            out.println(txtln);
          if (Math.abs(Diff) > 1.0) {
//              System.out.println("Diff:" + Diff + ", "+ ZLoc + ", "+ ZLoc2 + ", ");
            System.out.println("Diff:" + Diff + ", " + Diff / ZLoc2 + ", ");
          }
        }
        txtln = Circus.ObjPnt(XLoc, YLoc, ZLoc);
        out.println(txtln);
      }
    }
    out.close();

    System.out.println();
  }
  /* ******************************************************************************* */
  public static void DiveRace() {
    double Height0Prev, Height0Now, Height0Next;
    double Height1Prev, Height1Now, Height1Next;
    double DistancePrev, DistanceNow, DistanceNext;
    double Step = 1.0, BoxScanWidth = 200.0;

    Diver diver0 = new Diver();
    Diver diver1 = new Diver();

//    diver0.SetBaseND(new double[]{0.0, 0.0, 0.0, 0.0});
//    diver1.SetBaseND(new double[]{30.0, 50.0, 150.0, 0.0});
    
    diver0.SetBaseND(new double[]{30.0, 50.0, 150.0, 0.0});
    diver1.SetBaseND(new double[]{0.0, 0.0, 0.0, 0.0});

    DistancePrev = 0.0;// first 2 heights are xy coords, third is z
    DistanceNow = Step;
    DistanceNext = DistanceNow + Step;

    Height1Prev = diver0.GetHeight(DistancePrev);// seed values
    Height1Now = diver0.GetHeight(DistanceNow);
    Height1Next = diver0.GetHeight(DistanceNext);// not used

    System.out.println("Gold, Imitation, Diff, Diff / Gold, ");
    while (DistanceNext < BoxScanWidth) {
      Height0Prev = diver0.GetHeight(DistancePrev);
      Height0Now = diver0.GetHeight(DistanceNow);
      Height0Next = diver0.GetHeight(DistanceNext);

      Height1Next = diver1.FindNextHeight(Height1Prev, Height1Now);
      double Diff = Height0Next - Height1Next;

      // print here
      System.out.println("" + Height0Next + ", " + Height1Next + ", " + Diff + ", " + Diff / Height0Next + ", ");

      DistancePrev = DistanceNow;
      DistanceNow = DistanceNext;
      DistanceNext += Step;

      Height1Prev = Height1Now;
      Height1Now = Height1Next;
    }
    System.out.println();
  }
  /* ******************************************************************************* */
  public static void MapHeights() {
    int ImgGridSize = 125;// 200;// 800;
    int CellWdt = 1, CellHgt = CellWdt;
    int ImgWidth = ImgGridSize * CellWdt, ImgHeight = ImgGridSize * CellHgt;
    // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed into integer pixels
    BufferedImage BufImg = new BufferedImage(ImgWidth, ImgHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics2D gr2 = BufImg.createGraphics();

    Diver diver = new Diver();
    Diver diverfit = new Diver();

    double Step = 1.0, BoxScanWidth = 2.0;// 20.0;
    if (NDims == 3) {// crazy stupid if then
      BoxScanWidth = 4.0;// good for 3D
//      Step = 0.01;// good for 3D
      Step = 0.1;
    } else if (NDims == 4) {
      BoxScanWidth = 1.0;// good for 4D
      Step = 0.01;// good for 4D
      Step = 0.1;

      BoxScanWidth = 4.0;// testing fitter 
      Step = 1.0;
      if (false) {
        BoxScanWidth = 0.4;// fine grain
        Step = 0.01;
      }
    }
    //diverfit.SetBaseND(new double[]{0.0, BoxScanWidth, BoxScanWidth, BoxScanWidth});
    diverfit.SetBaseND(new double[]{0.0, 0.0, 0.0, 0.0});

    double ZLoc = 0;
    double HeightPrev, HeightNow, HeightNext;
    double DistancePrev, DistanceNow, DistanceNext;
    double HeightLimit = 10.0;// fake infinity
    double Expand = 10.0;// higher rez plot, more xy points

    String NewLine = "\n";

    PrintWriter out = Open_File("PntCloud.obj");

    out.println("o Cloud");

    Fitter fitter = new Fitter();

//    StringBuilder sb = new StringBuilder();
//    sb.append("o Cloud" + NewLine);
    for (int Trials = 0; Trials < 1; Trials++) {//100
      for (double WLoc = 0.0; WLoc < BoxScanWidth; WLoc += Step) {
//      for (double WLoc = 0.0; WLoc < Step; WLoc += Step) {
//    for (double WLoc = 0.0; WLoc < 1; WLoc += 1) {
        System.out.println("W:" + WLoc + ", ");
        for (double XLoc = 0.0; XLoc < BoxScanWidth; XLoc += Step) {
//        for (double XLoc = 0.0; XLoc < Step; XLoc += Step) {
          System.out.println("X:" + XLoc + ", ");
          for (double YLoc = 0.0; YLoc < BoxScanWidth; YLoc += Step) {
//          for (double YLoc = 0.0; YLoc < Step; YLoc += Step) {
            if (NDims == 3) {// crazy stupid if then
              diver.SetBaseND(new double[]{XLoc, YLoc, ZLoc});
            } else if (NDims == 4) {
              diver.SetBaseND(new double[]{WLoc, XLoc, YLoc, ZLoc});
            }
            DistancePrev = 0.0;// first 2 heights are xy coords, third is color
            DistanceNow = Step;
            DistanceNext = DistanceNow + Step;
            while (DistanceNext < BoxScanWidth) {
              HeightPrev = diver.GetHeight(DistancePrev);
              HeightNow = diver.GetHeight(DistanceNow);
              HeightNext = diver.GetHeight(DistanceNext);

              double TestChanger = diver.FindNextHeight(HeightPrev, HeightNow);
              double TestFitter = diverfit.FindNextHeight(HeightPrev, HeightNow);
              double Diff = HeightNext - TestFitter;

//              if (!Similar(TestChanger, TestFitter)) {
              if (!Similar(HeightNext, TestFitter)) {
//                System.out.println("Height:" + HeightNext + ", " + TestFitter + ", ");
                System.out.println("Diff:" + Diff + ", ");
              }

              fitter.Train(HeightPrev, HeightNow, HeightNext);

              String txtln = Circus.ObjPnt(HeightPrev, HeightNow, HeightNext);
              out.println(txtln);
//            sb.append(txtln + NewLine);

              HeightPrev *= Expand;
              HeightNow *= Expand;

              if (HeightNext < HeightLimit) {
                //Color col = Globals.ToColorWheel(HeightNext / HeightLimit);
                Color col = Globals.ToRainbow(HeightNext / HeightLimit);
                gr2.setColor(col);
              } else {
                gr2.setColor(Color.BLACK);
              }
              gr2.fillRect((int) (HeightPrev * CellWdt), (int) (HeightNow * CellHgt), CellWdt, CellHgt);
              //System.out.println("" + HeightPrev + ", " + HeightNow + ", " + HeightNext + ", ");

              DistancePrev = DistanceNow;
              DistanceNow = DistanceNext;
              DistanceNext += Step;
            }
          }
        }
      }
    }
    String FPath = "";//.\\";

    out.close();

//    try {
//      out.println(sb.toString());
//    } catch (Exception ex) {
//      System.out.println(ex.getStackTrace());
//    }
    try {
      ImageIO.write(BufImg, "png", new File(FPath + "HeightMap.png"));
    } catch (IOException ie) {
      ie.printStackTrace();
    }

    fitter.Print_Box(FPath + "fitterbox.obj");

//    diver.TestFitter(1.0, 2.0, 3.0, 4.0, fitter);
//    diver.TestFitter(0.0, 0.0, 0.0, 0.0, fitter);
    diver.TestFitter(1.0, 1.0, 1.0, 1.0, fitter);

    fitter.Recur(1.0, 2.0);

    System.out.println();
  }
  /* ********************************************************************************* */
  public static void MarchGrid() {// 2 dimensional function surface of 1D functions added together
    int GridSize = 200;// 800;
    int CellWdt = 2, CellHgt = CellWdt;
    int width = GridSize * CellWdt, height = GridSize * CellHgt;
    double CloseX, CloseY;
    // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed into integer pixels
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D ig2 = bi.createGraphics();

    /*
     Next: save grid values to 2D array, then take all diagonal curves and print each diag curve in its own column.
     Only do upper half, triangle.
     Import into spreadsheet and graph.
     */
    double HeightLimit = 100.0;
    double CloseFactor = 1000.0;
    for (int YCnt = 0; YCnt < GridSize; YCnt++) {
      CloseY = CloseFactor / YCnt;
      for (int XCnt = 0; XCnt < GridSize; XCnt++) {
        CloseX = CloseFactor / XCnt;
        double Height = CloseX + CloseY;
        if (Height < HeightLimit) {
          Color col = Globals.ToRainbow(Height / HeightLimit);
          //Color col = Globals.ToColorWheel(Height / 1.0);
          ig2.setColor(col);
        } else {
          ig2.setColor(Color.BLACK);
        }
        ig2.fillRect(XCnt * CellWdt, YCnt * CellHgt, CellWdt, CellHgt);
        //System.out.print(CloseX + ", " + CloseY + ", " + Height + ", ");
        //System.out.print(Height + ", ");
      }
      //System.out.println();
    }
    String FPath = ".\\";
    try {
      ImageIO.write(bi, "png", new File(FPath + "GridSum2.png"));
    } catch (IOException ie) {
      ie.printStackTrace();
    }
    System.out.println("");
  }
}
