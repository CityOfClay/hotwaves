/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HotWaves;

import static HotWaves.Circus.Open_File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author
 *
 * Use gradient descent to fit a curve (plane)
 */
public class Fitter {
  public int VectNDims = 3;
  public double[] Weights = null;
  public double[] InFire = null;
//  public double OutFire;
  public double LRate = 0.3;//0.1;//0.3;
  public double Base = 1.0;//0.0;//0.0;//1.0;
  /* ********************************************************************** */
  public Fitter() {
    Weights = new double[VectNDims];
    InFire = new double[VectNDims];
    //RandWeights();
  }
  /* ********************************************************************** */
  void RandWeights() {
    Random rand = new Random();
    for (int cnt = 0; cnt < VectNDims; cnt++) {
      this.Weights[cnt] = (rand.nextDouble() * 2.0) - 1.0;
    }
  }
  /* ********************************************************************** */
  void Set_Infire3D(double In0, double In1, double In2) {
    this.InFire[0] = In0;
    this.InFire[1] = In1;
    this.InFire[2] = In2;
  }
  /* ********************************************************************** */
  void Set_Infire(double[] Vector) {
    System.arraycopy(Vector, 0, this.InFire, 0, VectNDims);
  }
  /* ********************************************************************** */
  void Train(double In0, double In1, double Goal) {
    double In2 = Base;
    if (Math.abs(In0) < Double.MAX_VALUE) {
      if (Math.abs(In1) < Double.MAX_VALUE) {
        if (Math.abs(Goal) < Double.MAX_VALUE) {
          Set_Infire3D(In0, In1, In2);
          double fire = this.Fire();
          double Corrector = Goal - fire;
          Apply_Corrector(Corrector);
        }
      }
    }
  }
  /* ********************************************************************** */
  public double Fire() {
    double fire, Sum = 0;
    for (int cnt = 0; cnt < VectNDims; cnt++) {
      fire = this.InFire[cnt] * this.Weights[cnt];
      Sum += fire;
    }
    return Sum;
  }
  /* ********************************************************************** */
  void Apply_Corrector(double Corrector) {
    double num, fire;
    double ModCorrector = Corrector * LRate;
    int siz = VectNDims;
    /*
    first we get the whole input vector and unitize it (sum of squares?). that becomes the recognition vector.
    then mult the recog vector by the corrector (and by lrate) and add it to my input weights.
     */
    double SumSq = 0.0;
    for (int cnt = 0; cnt < siz; cnt++) {
      fire = this.InFire[cnt];
      SumSq += fire * fire;
    }
    for (int cnt = 0; cnt < siz; cnt++) {
      fire = this.InFire[cnt];
      num = fire / SumSq;
      this.Weights[cnt] += num * ModCorrector;
    }
  }
  /* ********************************************************************** */
  void Print_Box(String FileName) {
    PrintWriter out = Open_File(FileName);
    out.println("o Cloud");

    double XLoc, YLoc, ZLoc, Step = 1.0;//0.1;
    Step = 50.0;
    double Limit = Step + 1;
    for (XLoc = 0; XLoc < Limit; XLoc += Step) {
      for (YLoc = 0; YLoc < Limit; YLoc += Step) {
        Set_Infire3D(XLoc, YLoc, Base);
        ZLoc = this.Fire();
        String txtln = Circus.ObjPnt(XLoc, YLoc, ZLoc);
        out.println(txtln);
      }
    }
    out.close();
  }
  /* ********************************************************************** */
  void Recur(double Seed0, double Seed1) {
    double XLoc, YLoc, ZLoc, Step = 1.0;//0.1;
    System.out.println("Fitter");
    PrintWriter out = Open_File("FitterCloud.obj");
    out.println("o FitterCloud");

    //double Seed = 1.0;
    XLoc = Seed0;
    YLoc = Seed1;
    //YLoc = XLoc + 1.0;// Step;// ????
    for (int cnt = 0; cnt < 100; cnt++) {
      this.Set_Infire3D(XLoc, YLoc, Base);
      ZLoc = this.Fire();
      // print here
      System.out.println("" + XLoc + ", " + YLoc + ", " + ZLoc + ", ");
      String txtln = Circus.ObjPnt(XLoc, YLoc, ZLoc);
      out.println(txtln);
      XLoc = YLoc;
      YLoc = ZLoc;
    }
    out.close();
    System.out.println("");
  }
}
