package HotWaves;

import java.awt.Color;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author MultiTool
 */
public class Globals {
  public static int SampleRate = 44100;
  public static int SampleRateTest = 100;
  public static double BaseFreqC0 = 16.3516;// hz
  public static double BaseFreqA0 = 27.5000;// hz
  public static double MiddleC4Freq = 261.626;// hz
  public static double TwoPi = Math.PI * 2.0;// hz
  public static double Fudge = 0.00000000001;
  public static Random RandomGenerator = new Random();
  public static String PtrPrefix = "ptr:";// for serialization
  public static String ObjectTypeName = "ObjectTypeName";// for serialization
  public static final double CpeedOLight = 1.0;
  public static final double CSqared = CpeedOLight * CpeedOLight;
  public static final double GravConst = 6.67408e-11;// 6.67408(31) * 10−11;// 0.00001;// 1.0;// 
  /* ********************************************************************************* */
  public static double Sigmoid(double xin) {
    double OutVal;
    OutVal = xin / Math.sqrt(1.0 + xin * xin);// symmetrical sigmoid function in range -1.0 to 1.0.
    return OutVal;
    /*
     double power = 2.0; 
     OutVal = xin / Math.pow(1 + Math.abs(Math.pow(xin, power)), 1.0 / power);
     */
  }
  /* ********************************************************************************* */
  public static Color ToNegPos(double Fraction) {// negative is blue, positive is red 
    Fraction = Sigmoid(Fraction * 0.06);
    Fraction = (Fraction + 1.0) / 2.0;
    return new Color((float) (Fraction), (float) 0.0, (float) (1.0 - Fraction));
  }
  /* ********************************************************************************* */
  public static Color ToAlpha(Color col, int Alpha) {
    return new Color(col.getRed(), col.getGreen(), col.getBlue(), Alpha);// rgba 
  }
  /* ********************************************************************************* */
  public static Color ToRainbow(double Fraction) {
    if (Fraction < 0.5) {
      Fraction *= 2.0;// red to green
      return new Color((float) (1.0 - Fraction), (float) Fraction, 0);
    } else {
      Fraction = Math.min((Fraction - 0.5) * 2, 1.0);// green to blue
      return new Color(0, (float) (1.0 - Fraction), (float) Fraction);
    }
  }
  /* ********************************************************************************* */
  public static Color ToColorWheel(double Fraction) {
    Fraction = Fraction - Math.floor(Fraction); // remove whole number part if any
    double StartBig, DeadAir = 0.0;
    if (Fraction < (1.0 / 3.0)) {
      Fraction *= 3.0;// now range 0 to 1
      StartBig = 1.0 - Fraction;
      return new Color((float) StartBig, (float) Fraction, (float) DeadAir);// red to green
    } else if (Fraction < (2.0 / 3.0)) {
      Fraction = (Fraction - (1.0 / 3.0)) * 3.0;// now range 0 to 1
      StartBig = 1.0 - Fraction;
      return new Color((float) DeadAir, (float) StartBig, (float) Fraction);// green to blue
    } else {
      Fraction = (Fraction - (2.0 / 3.0)) * 3.0;// now range 0 to 1
      StartBig = 1.0 - Fraction;
      return new Color((float) Fraction, (float) DeadAir, (float) StartBig);// blue to red
    }
  }
  /* ********************************************************************************* */
  public static void VectorFill(double[] array, double value) {//https://stackoverflow.com/questions/9128737/fastest-way-to-set-all-values-of-an-array
    int len = array.length;
    if (len > 0) {
      array[0] = value;
    }
    for (int i = 1; i < len; i += i) {
      System.arraycopy(array, 0, array, i, ((len - i) < i) ? (len - i) : i);
    }
  }
}
