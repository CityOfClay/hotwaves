package HotWaves;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author MultiTool
 */
public class HotWaves {
  /**
   * @param args the command line arguments
   */
  /* ********************************************************************************* */
  public static void main(String[] args) {
    MainGui mg = new MainGui();
    mg.Init();
  }
  /* ********************************************************************************* */
  public double StaticSum = 0.0;
  public int GenCnt = 0;
  public ArrayList<CellRow> Rows;
  double Angle = 0.0;
  CellRow OscRow;
  /* ********************************************************************************* */
  public HotWaves() {
    if (false) {
      Circus.DiveRace();
      System.out.println();
    }
    if (false) {
      Circus.HeightGrid();
      System.out.println();
    }
    if (false) {
      Circus.MapHeights();
      System.out.println();
    }
    if (false) {
      Circus circus = new Circus();
      circus.PlotTest();
      Circus.MapHeights();
      System.out.println();//0.6699999999999999
    }
    if (false) {
      //WritePng();
      if (true) {
        Circus.MarchGrid();
      }
      GravFieldTest();
      System.out.println();
    }
    if (false) {
      CircleTest();
      System.out.println();
    }
    if (false) {
      double Mass = 1.0;
      double Distance = 0.0;
      double TimeRate = 0.0;
      for (int cnt = 0; cnt < 100; cnt++) {
        Distance = cnt;
        TimeRate = RelGravTime(1.0, Mass, Distance);
        System.out.println(TimeRate);
      }
      System.out.println();
    }
    if (false) {
      WaveLineTest();
      WaveTestTest();
    }
    if (true) {
//      this.Init(32, 1);
//      this.Init(1, 1);
//      this.Init(2, 2);
//      this.Init(3, 3);
//      this.Init(4, 4);
//      this.Init(5, 5);
//      this.Init(6, 6);
//      this.Init(8, 8);
//      this.Init(11, 11);
//      this.Init(12, 12);
//      this.Init(16, 16);
//      this.Init(32, 32);
//      this.Init(64, 64);
//      this.Init(128 * 2, 64);
//      this.Init(128, 128);
//      this.Init(256, 256);
//      this.Grav_Test_Setup(128, 128);
      this.Fiber_Optic_Setup(128, 128);
    }
  }
  /* ********************************************************************************* */
  public static class CellTest {
    public double Amp_Prev = 0.0, Amp = 0.0, Amp_Next = 0.0;
    public double Speed = 0.0, Speed_Next = 0.0;
    public double Damp = 1.0; //0.99;//0.5;//
    public double Leak = 0.0;
//    public static double TRate = 0.1;
//    public static double TRate = 0.05;
    public double TRate = 0.01;
    /* ********************************************************************************* */
    public void Calc_Amp(double OtherAmp) {
      double Delta = OtherAmp - this.Amp;// Spring force
      Delta *= TRate;
      this.Leak = Delta;
      this.Amp_Next = this.Amp + this.Speed;
      this.Amp_Next = this.Amp_Next + Delta;
      this.Speed_Next = this.Amp_Next - this.Amp;
      this.Speed_Next *= this.Damp;
    }
    public void Rollover() {
      this.Amp_Prev = this.Amp;
      this.Amp = this.Amp_Next;
      this.Speed = this.Speed_Next;
    }
    public void Calc_Vel() {
      this.Speed = this.Amp_Next - this.Amp;// if we do this before Rollover
      //this.Vel = this.Amp - this.Amp_Prev;// if we do this after Rollover
    }
    public double Interpolate(double Count, double Level) {
      double Dist = Level - this.Amp_Prev;
      double Height = this.Amp - this.Amp_Prev;
      // width is 1
      double Fract = Dist / Height;
      double Time = Count + Fract;
      return Time;
    }
    public boolean Crosses_Up(double Level) {
      if (this.Amp_Prev < Level && Level < this.Amp) {// going up
        return true;
      } else {
        return false;
      }
    }
    public boolean Is_Peak() {
      if (this.Amp_Prev < this.Amp && this.Amp > this.Amp_Next) {// top peak
        return true;
      } else if (this.Amp_Prev > this.Amp && this.Amp < this.Amp_Next) {// bottom peak
        return true;
      } else {
        return false;
      }
    }
    /* ********************************************************************************* */
    public void Add_Speed() {
      this.Amp_Prev = this.Amp;
      this.Amp += this.Speed;
    }
    public void Print_Me() {
//      System.out.print(" Amp:" + this.Amp);
      System.out.print(" " + this.Amp + ", ");
    }
  }
  /* ********************************************************************************* */
  public double LogX(double Value, double Base) {
    return (Math.log(Value) / Math.log(Base));
  }
  /* ********************************************************************************* */
  public double UnLogX(double Value, double Base) {
    return Math.pow(Base, Value);
  }
  /* ********************************************************************************* */
  public double Log2(double Value) {
    return LogX(Value, 2.0);
  }
  /* ********************************************************************************* */
  public double LogMap(double Halver, double Base) {
    double InvHalver, Log, InvLog;
    InvHalver = 1.0 / Halver;
    Log = LogX(InvHalver, Base);
    InvLog = 1.0 / Log;
    return InvLog;
  }
  /* ********************************************************************************* */
  public double Flipcrement(double StartVal, double StepDistance) {
    //return 1.0 / StartVal;// test - fail
    //return StartVal*2;// test - succeed
    //return StartVal + 2.0;// test - succeed
    return 1.0 / ((1.0 / StartVal) + StepDistance);// increment  
    //return 1.0 / ((StartVal) + StepDistance);// increment  
  }
  /* ********************************************************************************* */
  public double IncrementSnowBall(double SnowBall, double Base, double NumSamples, double StepDistance) {
    double SnowBallRoot = Math.pow(SnowBall, 1.0 / NumSamples);// the NumSamples root of SnowBall

    double Stage1 = LogX(SnowBallRoot, Base);// through the log
    double Stage2 = Flipcrement(Stage1, StepDistance);// increment
    double Stage3 = UnLogX(Stage2, Base);// backward out of the log

    // alternative, sum logs and unlog instead of unlog and multiply - same results
    double Alt0 = Stage2 * NumSamples;
    double Alt1 = UnLogX(Alt0, Base);

    double SnowBallNext = Math.pow(Stage3, NumSamples);// Put all the samples back together to make the next SnowBall.
    return SnowBallNext;
  }
  /* ********************************************************************************* */
  public void GravFieldTest() {// 1 dimensional
    int XCnt, YCnt;
    double Dist0, Dist1, Closeness0, Closeness1;
    double SumCloseness, InvSumCloseness;
    double Base = 2.0;//1.0001;// 2.0;
    double Halver0 = 1.0, InvLog0;
    double Halver1 = 0.25, InvLog1;// 2 cells behind you
    double HalverSum, InvLogSum;
//    double bleh = Math.log(Math.E); System.out.println(bleh);// meaning: what power of E equals bleh
// https://en.wikipedia.org/wiki/Natural_logarithm  // ln(x*y) = ln(x) + ln(y) 
    if (true) {// this only makes sense after we have computed SnowBall
      double MyBase = 2.0;//1.001;//
      double NumSamples = 2.0;
      double StepDistance = 1.0;//0.0;//
      double SnowBall = 123.45;
      double Stage0 = Math.pow(SnowBall, 1.0 / NumSamples);// the NumSamples root of SnowBall

      double Stage1 = LogX(Stage0, MyBase);// through the log
      double Stage2 = Flipcrement(Stage1, StepDistance);// increment
      double Stage3 = UnLogX(Stage2, MyBase);// backward out of the log

      double SnowBallNext = Math.pow(Stage3, NumSamples);// Put all the samples back together to make the next SnowBall.
      System.out.println(SnowBallNext + ", ");
    }

    if (true) {
      Random rand = new Random();
      double MyBase = 2.0;//1.001;//
      double StepDistance = 1.0;
      double NumSamples = 2;
      double Close0 = 0.25;
      double Close1 = 0.25 / 2.0;
      //Close1 = rand.nextDouble();
      Dist0 = 1.0 / Close0;
      Dist1 = 1.0 / Close1;
      double UnLog0 = Math.pow(MyBase, Close0);// UnLog0 = 1.189207115002721
      double UnLog1 = Math.pow(MyBase, Close1);// UnLog1 = 1.0905077326652577
      double SnowBall = UnLog0 * UnLog1;// SnowBall = 1.2968395546510096
      double SnowBallRoot = Math.pow(SnowBall, 1.0 / NumSamples);// the NumSamples root of SnowBall
      double bleh = SnowBallRoot * SnowBallRoot;
      // Here we need to transform SnowBall in a way that propagates it to a greater distance.
      double SnowBallNext = IncrementSnowBall(SnowBall, MyBase, NumSamples, StepDistance);
      double LogSnowBallNext = LogX(SnowBallNext, MyBase);// 0.31578947368421023
      // Stage2 is 0.1578947368421052 * 2 = 0.31578947368

      // double LogProd = LogX(SnowBall * (MyBase * MyBase), MyBase);// wrong
      double LogSnowBall = LogX(SnowBall, MyBase);// LogProd = 0.375
      double LogFarther;// = ???IncrementSnowBall
      // Closeness would be 1/(1+(1/Close)) for both Closeness
      //double TrueFarther = (1.0 / (1.0 + (Dist0))) + (1.0 / (1.0 + (Dist1)));// TrueFarther = 0.3111111111111111

      double Flip0 = Flipcrement(Close0, StepDistance);
      double Flip1 = Flipcrement(Close1, StepDistance);
      double AvgFlip = (Flip0 + Flip1) / 2.0;
      double TrueFarther = Flip0 + Flip1;// TrueFarther = 0.3111111111111111
      double TrueSum = (Close0) + (Close1);// TrueSum = 0.375
      System.out.println(TrueSum + ", " + LogSnowBall + ", " + TrueFarther + ", " + SnowBallNext + ", ");
    }
    if (true) {// UnLog is the answer we save?
      double MyBase = 2.0;
      double Raw = 0.25;
      double UnLog = Math.pow(MyBase, Raw);// UnLog = 1.189207115002721
      double Log = LogX(UnLog, MyBase);
      double LogProd = LogX(UnLog * UnLog, MyBase);// LogProd = 0.49999999999999994
      double LogSum = LogX(UnLog, MyBase) + LogX(UnLog, MyBase);// LogSum = 0.49999999999999994
      System.out.println(Raw + ", " + UnLog + ", " + Log + ", " + LogProd + ", " + LogSum + ", ");
      /*
       next test:
       create unlog from falling value, snowball multiply unlogs.  log of unlog is the fraction.  sum of the fractions (1/dist) is good. 
       log of snowball is sum of all fractions. 
       is snowball the falling value? no.
       snowball multiply unlogs.  but with distance, multiply snowball by 1/2 each step?  mult snowball by 1/2 to the power of number of unlogs, at each step?
       */
    }
    if (false) {// next test: now we can create the inverse of the logs, add them and compare with product of InvPowers
      double Power = LogX(16.0, 2.0);
      double InvPower = 1.0 / Power;// InvPower is 0.25 or 1/4
      double UnPower = LogX(InvPower, 16.0);// UnPower is -0.5
      // We want InvPower to be the output of a single log operation.
      // So  ha = (blah^InvPower)  what to the 1/4 equals ha?

      double LogSum = UnPower + UnPower;// adding to itself because I am lazy
      double LogProd = LogX(InvPower * InvPower, 16.0);
      System.out.println(Power + ", " + UnPower + ", " + LogSum + ", " + LogProd + ", ");
    }

    for (XCnt = 0; XCnt < 10; XCnt++) {
      Dist0 = XCnt;
      Dist1 = Dist0 + 2.0;// 2 cells behind you
      Closeness0 = 1.0 / Dist0;
      Closeness1 = 1.0 / Dist1;
      SumCloseness = Closeness0 + Closeness1;
      InvSumCloseness = 1.0 / SumCloseness;

      if (false) {
        double InvHalver, HLog;
        InvHalver = 1.0 / Halver0;
        HLog = LogX(InvHalver, Base);
//      HLog = LogN(Halver, HBase);
        InvLog0 = 1.0 / HLog;
        System.out.println(Halver0 + ", " + InvHalver + ", " + HLog + ", " + InvLog0);
      }
      if (true) {// log of the product equals the sum of the logs
        double Log0 = LogX(Halver0, Base);
        double Log1 = LogX(Halver1, Base);
        double LogSum = Log0 + Log1;
        double LogProd = LogX(Halver0 * Halver1, Base);
        System.out.println(LogSum + ", " + LogProd + ", ");
      }
      if (false) {
        InvLog0 = LogMap(Halver0, Base);
        InvLog1 = LogMap(Halver1, Base);
        HalverSum = Halver0 + Halver1;
        double TrueSum = InvLog0 + InvLog1;
        InvLogSum = LogMap(HalverSum, Base);
        System.out.println(Halver0 + ", " + InvLog0 + ", " + InvLog1 + ", " + InvLogSum + ", " + SumCloseness + ", " + TrueSum);
      }
      //System.out.println(Sum + ", " + InvSum);
      //System.out.print(Sum + ", ");
      Halver0 = Halver0 / Base;
      Halver1 = Halver1 / Base;
    }
    System.out.println();
  }
  /*
   1.0, 1.0, 0.0
   0.5, 2.0, 1.0
   0.25, 4.0, 2.0
   0.125, 8.0, 3.0
   0.0625, 16.0, 4.0
   0.03125, 32.0, 5.0
   0.015625, 64.0, 6.0
   0.0078125, 128.0, 7.0
   0.00390625, 256.0, 8.0
   0.001953125, 512.0, 9.0
   */
 /* ********************************************************************************* */
  public void CircleTest() {// 2 dimensional
    int Size = 20;
    double Dist, Closeness;
    for (int YCnt = 0; YCnt < Size; YCnt++) {
      for (int XCnt = 0; XCnt < Size; XCnt++) {
        Dist = Math.sqrt((XCnt * XCnt) + (YCnt * YCnt));
        Closeness = 1.0 / Dist;
//        System.out.print(Dist + ", ");
        System.out.print(Closeness + ", ");
      }
      System.out.println();
    }
  }
  /* ********************************************************************************* */
  public static class Record {
    public double Time;
    public double Amp;
    public double Speed;
    public double Leak;
  }
  /* ********************************************************************************* */
  public void WaveLineTest() {
    double Rate = 0.1;
    int NumSamples = 100000;// right number for frequency tests
    NumSamples = 1000;
    NumSamples = 100;
    CellTest cell_prev, cell, cell_next;
    ArrayList<CellTest> line = new ArrayList<CellTest>();
    int NumCells = NumSamples * 2;
    int MidCellDex = NumSamples;
    for (int ccnt = 0; ccnt < NumCells; ccnt++) {
      cell = new CellTest();
      line.add(cell);
      cell.TRate = Rate;
      cell.Amp = 0.0;//-1.0;
    }

    cell = line.get(MidCellDex);
    cell.Amp = 1.0;

    for (int cnt = 0; cnt < NumSamples; cnt++) {
      cell_prev = line.get(0);
      cell = line.get(1);
      for (int ccnt = 2; ccnt < NumCells; ccnt++) {
        cell_next = line.get(ccnt);
        double Avg = (cell_prev.Amp + cell_next.Amp) / 2.0;
        cell.Calc_Amp(Avg);
        cell_prev = cell;
        cell = cell_next;
      }
      for (int ccnt = MidCellDex; ccnt < MidCellDex + 2; ccnt++) {
        cell = line.get(ccnt);
        cell.Print_Me();
      }
      System.out.println();
      for (int ccnt = 0; ccnt < NumCells; ccnt++) {
        cell = line.get(ccnt);
        cell.Rollover();
      }
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public void WaveTest(double Rate) {
//    Rate *= Rate;// squared
    CellTest cell0 = new CellTest();
    CellTest cell1 = new CellTest();
    cell0.TRate = Rate;
    cell1.TRate = Rate;
    cell0.Amp = 1.0;
    cell1.Amp = -1.0;
    cell1.Damp *= 0.0;
    cell1.TRate = 1.0;
    double SeaLevel = (cell0.Amp + cell1.Amp) / 2.0;
    ArrayList<Record> Peak_Recs = new ArrayList<Record>();
    ArrayList<Record> Cross_Recs = new ArrayList<Record>();
    int NumSamples = 100000;// right number for frequency tests
    NumSamples = 1000;
    NumSamples = 100;
    for (int cnt = 0; cnt < NumSamples; cnt++) {
      double Avg = (cell0.Amp + cell1.Amp) / 2.0;
      cell0.Calc_Amp(cell1.Amp);
      cell1.Calc_Amp(cell0.Amp);
      if (cell0.Is_Peak()) {
        Record rec = new Record();
        rec.Time = cnt;
        rec.Amp = cell0.Amp;
        rec.Speed = cell0.Speed;
        rec.Leak = cell0.Leak;
        Peak_Recs.add(rec);
      }
      if (cell0.Crosses_Up(SeaLevel)) {
        Record rec = new Record();
        rec.Time = cell0.Interpolate(cnt, SeaLevel);
        Cross_Recs.add(rec);
      }
//      cell0.Calc_Vel();
//      cell1.Calc_Vel();
      cell0.Rollover();
      cell1.Rollover();
      if (true) {
        cell0.Print_Me();
        cell1.Print_Me();
        System.out.println();
      }
      if (false) {// dumb idea
        // sin squared plus cos squared equals constant
        //double AmpVel = (cell0.Amp * cell0.Amp) + (cell0.Vel * cell0.Vel);
        double AmpVel = (cell0.Leak * cell0.Leak) + (cell0.Speed * cell0.Speed);
        System.out.println("AmpVel:" + AmpVel);
      }
    }
    Record rec;
    if (false) {
      rec = Peak_Recs.get(0);
      double Prev_Amp = rec.Amp;
      for (int cnt = 1; cnt < Peak_Recs.size(); cnt++) {
        rec = Peak_Recs.get(cnt);
        double Ratio = rec.Amp / Prev_Amp;
        System.out.println("Ratio:" + Ratio);
        double AmpVel = rec.Amp + rec.Speed;
//      System.out.println("AmpVel:" + AmpVel);
        Prev_Amp = rec.Amp;
      }
    }
    double Sum_Wave_Length = 0.0;
    rec = Cross_Recs.get(0);
    double Prev_Time = rec.Time;
    for (int cnt = 1; cnt < Cross_Recs.size(); cnt++) {
      rec = Cross_Recs.get(cnt);
      double Delta = rec.Time - Prev_Time;
      Prev_Time = rec.Time;
      Sum_Wave_Length += Delta;
    }
    double Avg_Wave_Len = Sum_Wave_Length / (double) (Cross_Recs.size() - 1);
    System.out.println("Rate0:" + cell0.TRate + "Rate1:" + cell1.TRate);
    System.out.println("Avg_Wave_Len:" + Avg_Wave_Len);
    System.out.println("Frequency:" + 1.0 / Avg_Wave_Len);
    System.out.println();
  }
  /* ********************************************************************************* */
  public void WaveTestTest() {
    double Rate = 0.1;
//    Rate = 0.05;
//    Rate = 0.01;
//    Rate = 0.001;
    Rate = 0.25;
    for (int cnt = 0; cnt < 6; cnt++) {
      WaveTest(Rate);
      Rate *= 0.5;
//      Rate *= 0.1;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public void RightHalfWall() {
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    int HalfWidth = NumCols / 2;
    CellRow crow;
    CellBase cell;
    CellWall WallSeed = new CellWall();
    CellBt BtSeed = new CellBt();
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.FillWithCells(BtSeed, 0, HalfWidth);
      crow.FillWithCells(WallSeed, HalfWidth, NumCols);
    }
  }
  /* ********************************************************************************* */
  public void TimeField(int XCtr, int YCtr, int Radius, double CtrMass) {
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    int Left = XCtr - Radius, Right = XCtr + Radius;
    int Bottom = YCtr - Radius, Top = YCtr + Radius;
    Left = Math.max(0, Math.min(Left, NumCols));
    Right = Math.max(0, Math.min(Right, NumCols));
    Bottom = Math.max(0, Math.min(Bottom, NumRows));
    Top = Math.max(0, Math.min(Top, NumRows));
    CellRow crow;
    CellBase cell;
    double Distance, Dx, Dy, MassOverDist;
    for (int YCnt = Bottom; YCnt < Top; YCnt++) {
      crow = this.Rows.get(YCnt);
      for (int XCnt = Left; XCnt < Right; XCnt++) {
        cell = crow.Get(XCnt);
        Dx = (XCtr - cell.XDex);
        Dy = (YCtr - cell.YDex);
        Distance = Math.sqrt((Dx * Dx) + (Dy * Dy));
        if (Distance == 0.0) {
          Distance = 0.000000001;
        } else {// this never happens!
//          System.out.print("Distance:" + Distance + ", ");
        }
//        Distance *= 0.0000001;
//        Distance *= 0.00000001;// shrink distance a lot so we can see the effect
        Distance *= 0.000000005;// shrink distance a lot so we can see the effect
        MassOverDist = CtrMass / Distance;
        cell.Add_MassOverDistance(MassOverDist);
      }
    }
    System.out.println("TimeField done");
  }
  /* ********************************************************************************* */
  public void FiberOptic(int YMin, int YMax, double EdgeTime, double CoreTime) {
    int NumRows = this.Rows.size();
    int YMid = (YMin + YMax) / 2;
    int HalfHgt = YMid - YMin;
    double TimeRateRange = CoreTime - EdgeTime;
    CellRow crow;
    double FractAlong = 0.0, Rate = 0.0;
    int cnt = 0;
    while (cnt < YMin) {// outside of fiber
      crow = this.Rows.get(cnt);
      crow.Fill_TimeRate(EdgeTime);
      cnt++;
    }
    while (cnt < YMid) {// from edge to core of fiber
      crow = this.Rows.get(cnt);
      FractAlong = ((double) (cnt - YMin)) / (double) HalfHgt;
      Rate = (FractAlong * TimeRateRange) + EdgeTime;
      crow.Fill_TimeRate(Rate);
      cnt++;
    }
    while (cnt < YMax) {// from core to opposite edge of fiber
      crow = this.Rows.get(cnt);
      FractAlong = ((double) (cnt - YMid)) / (double) HalfHgt;
      FractAlong = 1.0 - FractAlong;
      Rate = (FractAlong * TimeRateRange) + EdgeTime;
      crow.Fill_TimeRate(Rate);
      cnt++;
    }
    while (cnt < NumRows) {// outside of fiber
      crow = this.Rows.get(cnt);
      crow.Fill_TimeRate(EdgeTime);
      cnt++;
    }
  }
  /* ********************************************************************************* */
  public void AllButOsc(int XLoc, int YLoc) {
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    CellRow crow;
    CellOsc OscSeed = new CellOsc();
    CellBt BtSeed = new CellBt();
    int cnt = 0;
    while (cnt < YLoc) {
      crow = this.Rows.get(cnt);
      crow.FillWithCells(BtSeed, 0, NumCols);
      cnt++;
    }
    {// create and fill the one row that has an oscillator
      crow = this.Rows.get(cnt);
      crow.FillWithCells(BtSeed, 0, XLoc);
      crow.FillWithCells(OscSeed, XLoc, XLoc + 1);
      crow.FillWithCells(BtSeed, XLoc + 1, NumCols);
      cnt++;
    }
    while (cnt < NumRows) {
      crow = this.Rows.get(cnt);
      crow.FillWithCells(BtSeed, 0, NumCols);
      cnt++;
    }
  }
  /* ********************************************************************************* */
  public void TimeRateRectangle(int MinX, int MinY, int MaxX, int MaxY, double Value) {
    int LastY = MaxY - 1, LastX = MaxX - 1;// mins are inclusive, maxes are exclusive
    CellRow crow;
    // First fill top and bottom lines of rectangle
    crow = this.Rows.get(MinY);
    crow.Fill_TimeRate(MinX, MaxX, Value);
    crow = this.Rows.get(LastY);
    crow.Fill_TimeRate(MinX, MaxX, Value);
    for (int RowCnt = MinY + 1; RowCnt < LastY; RowCnt++) {
      crow = this.Rows.get(RowCnt);
      crow.Get(MinX).Set_TRate(Value);// left vertical side
      crow.Get(LastX).Set_TRate(Value);// right vertical side
    }
  }
  /* ********************************************************************************* */
  public void TimeRateFrame() {// Make a frame around the grid where time rate is slower. 
    int NumCols = this.Rows.get(0).size();
    int NumRows = this.Rows.size();
    int MinX = 0, MinY = 0, MaxX = NumCols, MaxY = NumRows;
    int NumFrames = 10;// absorbent border is NumFrames thick
    NumFrames = 24;
//    NumFrames = 20;
    double Value = 1.0, FractAlong, FractFrom, StartingValue = 0.0;//10.0;
    for (int FrameCnt = 0; FrameCnt < NumFrames; FrameCnt++) {
      FractAlong = ((double) FrameCnt + StartingValue) / ((double) NumFrames + StartingValue);
      //FractAlong = FractAlong / (FractAlong + 10);
      FractFrom = 1.0 - FractAlong;
      Value = 1.0 - Math.pow(FractFrom, 3.0);
      TimeRateRectangle(MinX, MinY, MaxX, MaxY, Value);
      MinX++;// rectangle gets smaller each FrameCnt
      MinY++;
      MaxX--;
      MaxY--;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public void DampRectangle(int MinX, int MinY, int MaxX, int MaxY, double Value) {
    int LastY = MaxY - 1, LastX = MaxX - 1;// mins are inclusive, maxes are exclusive
    CellRow crow;
    // First fill top and bottom lines of rectangle
    crow = this.Rows.get(MinY);
    crow.Fill_Damps(MinX, MaxX, Value);
    crow = this.Rows.get(LastY);
    crow.Fill_Damps(MinX, MaxX, Value);
    if (false) {
      for (int RowCnt = MinY + 1; RowCnt < LastY; RowCnt++) {
        crow = this.Rows.get(RowCnt);
        crow.Get(MinX).Set_Damp(Value);// left vertical side
        crow.Get(LastX).Set_Damp(Value);// right vertical side
      }
    }
  }
  /* ********************************************************************************* */
  public void DampFrame() {// Make a frame around the grid to absorb waves so they don't reflect back.
    int NumCols = this.Rows.get(0).size();
    int NumRows = this.Rows.size();
    int MinX = 0, MinY = 0, MaxX = NumCols, MaxY = NumRows;
    int NumFrames = 10;// absorbent border is NumFrames thick
    NumFrames = 24;
//    NumFrames = 20;
    double Value = 1.0, FractAlong, FractFrom, StartingValue = 0.0;//10.0;
    for (int FrameCnt = 0; FrameCnt < NumFrames; FrameCnt++) {
      FractAlong = ((double) FrameCnt + StartingValue) / ((double) NumFrames + StartingValue);
      //FractAlong = FractAlong / (FractAlong + 10);
      FractFrom = 1.0 - FractAlong;
      Value = 1.0 - Math.pow(FractFrom, 3.0);
      DampRectangle(MinX, MinY, MaxX, MaxY, Value);
      MinX++;// rectangle gets smaller each FrameCnt
      MinY++;
      MaxX--;
      MaxY--;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public void Torus_Connect() {
    // Connect all cells with neighbors
    int NumRows = this.Rows.size();
    CellRow crow_prev, crow;
    int CasiAltura = NumRows - 1;
    crow = this.Rows.get(CasiAltura);// looped top to bottom
    for (int ycnt = 0; ycnt < NumRows; ycnt++) {
      crow_prev = crow;
      crow = this.Rows.get(ycnt);
      crow.ConnectCells();// connect and wrap horizontally
      crow.ConnectRows(crow_prev);// connect and wrap vertically
    }
  }
  /* ********************************************************************************* */
  public void Assign_DrawBox(double XOrg, double YOrg, double CellWidth, double CellHeight) {
    CellRow crow;
    int NumRows = this.Rows.size();
    for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
      crow = this.Rows.get(RowCnt);
      crow.Assign_DrawBox(XOrg, YOrg + (RowCnt * CellHeight), CellWidth, CellHeight);
    }
  }
  /* ********************************************************************************* */
  public void Create_Empty_Rows(int NumCols, int NumRows) {
    this.Rows = new ArrayList<CellRow>();
    CellRow crow;
    for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
      crow = new CellRow(NumCols);
      crow.Set_Dex(RowCnt);
      this.Rows.add(crow);
    }
  }
  /* ********************************************************************************* */
  public void CalcLorentzTimeFactor() {// bake in Lorentz time dilation based on M/D for each cell
    CellRow crow;
    int NumRows = this.Rows.size();
    for (int rcnt = 0; rcnt < NumRows; rcnt++) {
      crow = this.Rows.get(rcnt);
      crow.CalcTimeFactor();
    }
  }
  /* ********************************************************************************* */
  public void Grav_Test_Setup(int NumCols, int NumRows) {
    /*
    When running this, also make CellOsc.Update just a faucet
    and enable this.OscRow.Print_Me() snapshot
     */
    //this.Rows = new ArrayList<CellRow>();
    double XOrg = 10, YOrg = 10;
    double CellWidth = 6, CellHeight = 6;
    CellWidth = CellHeight = 4;
    if (true) {
      Create_Empty_Rows(NumCols, NumRows);
      int OscXLoc = NumCols / 2, OscYLoc = NumRows / 2;
      AllButOsc(OscXLoc, OscYLoc);
      this.OscRow = this.Rows.get(OscYLoc);
      Assign_DrawBox(XOrg, YOrg, CellWidth, CellHeight);
    }
    Torus_Connect();// Connect all cells with neighbors
    DampFrame();
    double slowtime = 0.00001;// 0.00000001;
    slowtime = 0.01;
    if (false) {
      TimeRateFrame();
    }
    if (true) {// this does not work right yet
      double Mass = 400.0;
      int Radius = 40000;// how many cells to change
      // to do: create a rectangular fence of point masses around osc
      if (false) {// time fields around point masses
        if (false) {
          TimeField(NumCols / 2, NumRows / 2, Radius, 0.01);// almost nonexistant mass to see just free waves
        } else {
          TimeField(NumCols / 2, NumRows / 2, Radius, Mass);
          TimeField((NumCols / 2) + 30, NumRows / 2, Radius, Mass / 2.0);
          //TimeField((NumCols * 3) / 4, NumRows / 4, Radius, Mass / 2.0);
        }
      }
    }
    CalcLorentzTimeFactor();
    System.out.println("");
    this.Zero_Total_Velocity();
    this.GenCnt = 0;
  }
  /* ********************************************************************************* */
  public void Fiber_Optic_Setup(int NumCols, int NumRows) {
    Create_Empty_Rows(NumCols, NumRows);
    double XOrg = 10, YOrg = 10;
    double CellWidth = 6, CellHeight = 6;
    int OscXLoc = NumCols / 4, OscYLoc = NumRows / 2;
//    int OscXLoc = NumCols / 4, OscYLoc = (NumRows / 2) - 15;
    AllButOsc(OscXLoc, OscYLoc);
    Torus_Connect();// Connect all cells with neighbors
    this.OscRow = this.Rows.get(OscYLoc);
    Assign_DrawBox(XOrg, YOrg, CellWidth, CellHeight);
    double slowtime = 0.25;
//    slowtime = Math.sqrt(slowtime);
    int YMin = (NumRows * 1) / 3;
    int YMax = (NumRows * 2) / 3;
    int FiberRadius = 10;//5;
    YMin = (NumRows / 2) - FiberRadius;
    YMax = (NumRows / 2) + FiberRadius;
    //FiberOptic(YMin, YMax, Math.sqrt(slowtime), Math.sqrt(slowtime * 0.1));//0.25//0.15
    FiberOptic(YMin, YMax, Math.sqrt(slowtime), Math.sqrt(slowtime * 0.01));// deeper well
    DampFrame();
    this.Zero_Total_Velocity();
    this.GenCnt = 0;
  }
  /* ********************************************************************************* */
  public void Init(int NumCols, int NumRows) {
    CellBt seed = new CellBt();
    //this.Rows = new ArrayList<CellRow>();
    double XOrg = 10, YOrg = 10;
    double CellWidth = 6, CellHeight = 6;
//    CellWidth = CellHeight = 20;
//    CellWidth = CellHeight = 30;
//    CellWidth = CellHeight = 20;
//    CellWidth = CellHeight = 10;
    CellWidth = CellHeight = 4;
    CellRow crow_prev, crow;

    if (true) {
      Create_Empty_Rows(NumCols, NumRows);
      int OscXLoc = NumCols / 4, OscYLoc = NumRows / 2;
//      int OscXLoc = NumCols / 2, OscYLoc = NumRows / 2;
      AllButOsc(OscXLoc, OscYLoc);
//      AllButOsc(0, 0);
      this.OscRow = this.Rows.get(OscYLoc);
      Assign_DrawBox(XOrg, YOrg, CellWidth, CellHeight);
    } else if (false) {
      for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
        crow = new CellRow(NumCols);
        crow.Set_Dex(RowCnt);
        this.Rows.add(crow);
      }
      RightHalfWall();
      Assign_DrawBox(XOrg, YOrg, CellWidth, CellHeight);
    } else { // -------------------------------
      for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
        crow = new CellRow(seed, NumCols);
        crow.Assign_DrawBox(XOrg, YOrg + (RowCnt * CellHeight), CellWidth, CellHeight);
        crow.Set_Dex(RowCnt);
        this.Rows.add(crow);
      }
    }
    Torus_Connect();// Connect all cells with neighbors
    switch (0) {
      case 0:
        //this.Diagonal_Fill_Amps();
        break;
      case 1:
        this.RandAmps();
        break;
      case 2:
        this.Fill_Amps();
        break;
      case 3:
        this.LeftQuarterAmps(10.0);
        //this.RighHalfDamp(0.9999);
        //this.RighHalfDamp(0.99);// 0.99 this looks good at 64x64. damping must be deep though.
//        this.RighHalfDamp(0.0);
        break;
      default:
        break;
    }

    DampFrame();

    double slowtime = 0.00001;// 0.00000001;
    slowtime = 0.01;
    if (false) {
      for (int rcnt = 0; rcnt < NumRows; rcnt++) {
        crow = this.Rows.get(rcnt);
        crow.Fill_TimeRate(slowtime);
      }
//      RighHalfTime(slowtime * 2.0);
//      RighHalfTime(slowtime * 0.5);
//      RighHalfTime(slowtime * 0.99);
//      RighHalfTime(0.9);
//      TimeRateFrame();
    }
    if (false) {// this works
      slowtime = 0.25;
      //slowtime = Math.sqrt(slowtime);
      int YMin = (NumRows * 1) / 3;
      int YMax = (NumRows * 2) / 3;
      int FiberRadius = 5;
      YMin = (NumRows / 2) - FiberRadius;
      YMax = (NumRows / 2) + FiberRadius;
      FiberOptic(YMin, YMax, Math.sqrt(slowtime), Math.sqrt(slowtime * 0.1));//0.25//0.15
    }
    if (true) {// this does not work right yet
      double Mass = 1.0;
      //Mass = 0.01;
      Mass = 1000.0;
      Mass = 400.0;
      int Radius = 40000;// how many cells to change
//      Radius = 5;

      if (true) {// time fields around point masses
        if (false) {
          TimeField(NumCols / 2, NumRows / 2, Radius, 0.01);// almost nonexistant mass to see just free waves
        } else {
          TimeField(NumCols / 2, NumRows / 2, Radius, Mass);
          TimeField((NumCols / 2) + 30, NumRows / 2, Radius, Mass / 2.0);
          //TimeField((NumCols * 3) / 4, NumRows / 4, Radius, Mass / 2.0);
        }
      }
      CalcLorentzTimeFactor();
      System.out.println("");
    }
    if (false) { // deaden borders with slow time
      slowtime = 0.001;// 0.00000001;
      slowtime = 0.000000001;
      if (true) {
        for (int rcnt = 0; rcnt < NumRows / 2; rcnt++) {
          crow = this.Rows.get(rcnt);
          crow.Fill_TimeRate(slowtime);// horizontal wall
        }
      } else {
        crow = this.Rows.get(0);
        crow.Fill_TimeRate(slowtime);// horizontal wall
        for (int ycnt = 0; ycnt < (NumRows); ycnt++) {// vertical wall
          crow = this.Rows.get(ycnt);
          crow.Fill_TimeRate(0, 1, slowtime);
        }
      }
    }
    if (false) {
      this.Fill_Gravs();
    }
    this.Zero_Total_Velocity();
    this.GenCnt = 0;
  }
  /* ********************************************************************************* */
  public void Update() {// runcycle
    int NumRows = this.Rows.size();
    CellRow crow;
    if (false) {
      this.Fill_Gravs();
    }
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.Update();
    }
    // this is a hack that we shouldn't have to do. 
    // for some reason the total 'sea level' in our wave tank slowly drifts either up or down to infinity.
    // this re-sets the level
    double Sum = this.Get_Sum();
//    this.Adjust_Sum(this.StaticSum - Sum);
//    this.Zero_Total_Velocity();
    if (true) {
      if (this.GenCnt % 100 == 0) {
        System.out.println("GenCnt:" + this.GenCnt + ", Sum:" + Sum);
      }
    }
    if (false) {// use this to snapshot amps of middle row 
      //if (this.GenCnt > 100 && this.GenCnt % 100 == 0) {
      int Wdt = this.OscRow.size();
      int HalfWdt = Wdt / 2;// Assume source is in the middle
      if (this.GenCnt <= Wdt) {
        this.OscRow.Print_Me();
      } else {
        System.out.println("");
      }
    }
    this.GenCnt++;
  }
  /* ********************************************************************************* */
  public void Rollover() {// set up for next cycle
    int NumRows = this.Rows.size();
    CellRow crow;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.Rollover();
    }
  }
  /* ********************************************************************************* */
  public void Zero_Total_Velocity() {
    /*
     When a wave table is created, the total movement upward of all cells may be greater or less than the movement downward.
     It becomes like a bed of mattress springs vibrating but also hurtling upward or downward through space.
     Physically realistic, but we don't need that extra motion. 
     */
    this.StaticSum = this.Get_Sum();
    this.Adjust_Sum(-this.StaticSum);// now amps sum to zero and prev amps ARE all zero
    // Now the sum of all speeds is zero, so amps and prev amps can be moved together to any new water level and speed will remain zero.
//    this.Adjust_Sum_All(this.StaticSum);
//    this.Add_To_All(100.0);
    this.StaticSum = this.Get_Sum();
    this.GenCnt = 0;
  }
  /* ********************************************************************************* */
  public void LeftQuarterAmps(double Value) {
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    int QuarterWayX = NumCols / 4;
    int HalfWayY = NumRows / 2;
    CellRow crow;
    CellBase cell;
    int XCnt, YCnt;
    for (YCnt = 0; YCnt < NumRows; YCnt++) {
      crow = this.Rows.get(YCnt);
      for (XCnt = 0; XCnt < NumCols; XCnt++) {
        cell = crow.Get(XCnt);
        cell.Set_Amp(-Value);
      }
    }
    for (YCnt = HalfWayY; YCnt < HalfWayY + 1; YCnt++) {
      crow = this.Rows.get(YCnt);
//      for (XCnt = 0; XCnt < NumCols; XCnt++) {
//        cell = crow.Get(XCnt);
//        cell.Set_Amp(-Value);
//      }
      cell = crow.Get(QuarterWayX);
      cell.Set_Amp(Value);
    }
  }
  /* ********************************************************************************* */
  public void RighHalfTime(double Value) {// sets TRate for all cells from middle to right in every row
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    int HalfWidth = NumCols / 2;
    CellRow crow;
    CellBase cell;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      for (int XCnt = HalfWidth; XCnt < NumCols; XCnt++) {
        cell = crow.Get(XCnt);
        cell.Set_TRate(Value);
      }
    }
  }
  /* ********************************************************************************* */
  public void RighHalfDamp(double Value) {// sets damping for all cells from middle to right in every row
    int NumRows = this.Rows.size();
    int NumCols = this.Rows.get(0).size();
    int HalfWidth = NumCols / 2;
    int RightCell = NumCols - 1;
    CellRow crow;
    CellBase cell;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      for (int XCnt = HalfWidth; XCnt < NumCols; XCnt++) {
        cell = crow.Get(XCnt);
        cell.Set_Damp(Value);
      }
    }
  }
  /* ********************************************************************************* */
  public void RandAmps() {
    int NumRows = this.Rows.size();
    CellRow crow;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.Rand_Amps();
    }
  }
  /* ********************************************************************************* */
  public void Fill_Gravs() {// initialize with box
    CellBase cell;
    int NumRows = this.Rows.size();
    CellRow crow;
    double BoxSize = 0.47;
    int BoxRadiusInt = 1;
    int BoxDiameterInt = BoxRadiusInt * 2;

    int BoxTop = (NumRows / 2) - BoxRadiusInt;
    int BoxBottom = (BoxTop + BoxDiameterInt);

//    int BoxTop = (int) (((double) NumRows) * BoxSize);
//    int BoxBottom = (NumRows - BoxTop);
    if (false) {
      for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
        crow = this.Rows.get(RowCnt);
        int NumCols = crow.size();
        int BoxLeft = (int) (((double) NumCols) * BoxSize);
        int BoxRight = (NumCols - BoxLeft);
        for (int CellCnt = 0; CellCnt < NumCols; CellCnt++) {
          cell = crow.Get(CellCnt);
          if ((BoxTop < RowCnt && RowCnt < BoxBottom) && (BoxLeft < CellCnt && CellCnt < BoxRight)) {// inside box
            cell.Fluid_Next = cell.Fluid = 1.0;
          } else {// background
            cell.Fluid_Next = cell.Fluid = 0.0;
          }
        }
      }
    }

    double NegOne = -1.0;
    double Zero = 0.0;
    double One = 1.0;
    if (true) {// zero out edges
      // top and bottom edges
      crow = this.Rows.get(0);
      crow.Fill_Grav(Zero);
      crow = this.Rows.get(NumRows - 1);
      crow.Fill_Grav(Zero);

      for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
        crow = this.Rows.get(RowCnt);
        int NumCols = crow.size();
        cell = crow.Get(0);
        cell.SetCell(Zero);
        cell = crow.Get(NumCols - 1);
        cell.SetCell(Zero);
      }
    }

    crow = this.Rows.get(0);
    int NumCols = crow.size();
    int BoxLeft = (NumCols / 2) - BoxRadiusInt;
    FillBox(BoxLeft - 5, BoxTop, One);
    FillBox(BoxLeft + 5, BoxTop, NegOne);

    if (false) {
      // fill in box
      for (int RowCnt = BoxTop; RowCnt < BoxBottom; RowCnt++) {
        crow = this.Rows.get(RowCnt);
        NumCols = crow.size();

        BoxLeft = (NumCols / 2) - BoxRadiusInt;
        int BoxRight = (BoxLeft + BoxDiameterInt);

//      int BoxLeft = (int) (((double) NumCols) * BoxSize);
//      int BoxRight = (NumCols - BoxLeft);
        for (int CellCnt = BoxLeft; CellCnt < BoxRight; CellCnt++) {
          cell = crow.Get(CellCnt);
          cell.SetCell(One);
        }
      }
    }
  }
  /* ********************************************************************************* */
  public void FillBox(int BoxLeft, int BoxTop, double Value) {
    CellBase cell;
    CellRow crow;
    int BoxRadiusInt = 1;
    int BoxDiameterInt = BoxRadiusInt;// * 2;

    int BoxBottom = (BoxTop + BoxDiameterInt);

    for (int RowCnt = BoxTop; RowCnt < BoxBottom; RowCnt++) {
      crow = this.Rows.get(RowCnt);

      int BoxRight = (BoxLeft + BoxDiameterInt);

      for (int CellCnt = BoxLeft; CellCnt < BoxRight; CellCnt++) {
        cell = crow.Get(CellCnt);
        cell.SetCell(Value);
      }
    }
  }
  /* ********************************************************************************* */
  public void Diagonal_Fill_Amps() {/// initialize with diagonal wave pattern
    int NumRows = this.Rows.size();
    CellRow crow;
    double AmpVal, Angle = 0.0;
    double Gain = 10.0;
    double HorizFreq = 1;
    double TwoPi = 2.0 * Math.PI;
    double Offset = 0;
    CellBase cell;
    for (int RowCnt = 0; RowCnt < NumRows; RowCnt++) {
      crow = this.Rows.get(RowCnt);
      int NumCols = crow.size();
      int FinalCol = NumCols - 1;
      int AngleDex = (int) Offset;
      for (int CellCnt = 0; CellCnt < NumCols; CellCnt++) {
        cell = crow.Get(CellCnt);
        Angle = HorizFreq * TwoPi * (((double) AngleDex) / (double) NumCols);
        AmpVal = Math.sin(Angle) * Gain;
        cell.Set_Amp(AmpVal);
        AngleDex = (AngleDex == FinalCol) ? 0 : ++AngleDex;// increment and wrap
        System.out.println("AngleDex:" + AngleDex);
      }
      Offset += 2.0;//0.5;
      System.out.println("********** Offset:" + Offset);
      System.out.println();
    }
  }
  /* ********************************************************************************* */
  public void Fill_Amps() {
    int NumRows = this.Rows.size();
    CellRow crow;
    int wlen = 3;//8;
    int half = wlen / 2;
    int cnt = 0;
    double AmpVal0 = 0.0, AmpVal1 = 0.0;
    double Angle0 = 0.0, Angle1 = 0.0;
    double Gain = 10.0;
    double Advance = 2;
    double TwoPi = 2.0 * Math.PI;
    for (cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      if (true) {
        Angle0 = Advance * TwoPi * (((double) ((cnt + 0) % NumRows)) / (double) NumRows);
        Angle1 = Advance * TwoPi * (((double) ((cnt + 1) % NumRows)) / (double) NumRows);

        AmpVal0 = Math.sin(Angle0) * Gain;
        AmpVal1 = Math.sin(Angle1) * Gain;

        crow.Fill_Amps(AmpVal0);
//      crow.Fill_Prev_Amps(AmpVal1);
//      this.Rows.get((cnt + 1) % NumRows).Fill_Prev_Amps(AmpVal);
      } else {
        if (cnt % wlen < half) {
          crow.Fill_Amps(100.0);
//          this.Rows.get((cnt + 1) % NumRows).Fill_Prev_Amps(AmpVal0);
        } else {
          crow.Fill_Amps(-100.0);
//          this.Rows.get((cnt + 1) % NumRows).Fill_Prev_Amps(-AmpVal0);
        }
      }
    }
  }
  /* ********************************************************************************* */
  public double Get_Sum() {// get sum of all row Amps
    int NumRows = this.Rows.size();
    double Sum = 0.0;
    CellRow crow;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      Sum += crow.Get_Sum();
    }
    return Sum;
  }
  /* ********************************************************************************* */
  public void Adjust_Sum(double Amount) {// raise or lower 'water level' of medium
    int NumRows = this.Rows.size();
    CellRow crow;
    Amount /= (double) NumRows;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.Adjust_Sum(Amount);
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    int NumRows = this.Rows.size();
    CellRow crow;
    for (int cnt = 0; cnt < NumRows; cnt++) {
      crow = this.Rows.get(cnt);
      crow.Draw_Me(ParentDC);
    }
  }
  /* ********************************************************************************* */
  public void TestNDex() {
    int[] dimlims = {1, 2};
    /*
     Figure out a way to index N dimensions on up so it scales easily
     for dimcnt = ndims down to 0{
     . 
     }
    
     */
  }
  /* ********************************************************************************* */
  public void updateEMWave1(Graphics realg) {// http://www.falstad.com/emwave1/
  }
  /* ********************************************************************************* */
  public static double RelVel(final Vector Vel0, final Vector Vel1) {
    // Vel0 and Vel1 always have magnitude C and C, repectively. ;)
    Vector VDiff = new Vector();
    VDiff.Copy_From(Vel0);
    VDiff.Subtract(Vel1);
    double DeltaV = VDiff.Magnitude();// just get linear delta V
    double VSq = DeltaV * DeltaV;
    // http://hyperphysics.phy-astr.gsu.edu/hbase/Relativ/veltran.html
    double RelV = 1.0 / Math.sqrt(1.0 - (VSq / Globals.CSqared));// 1 / sqrt(1 - (Vsq/Csq));
    return RelV;
  }
  /* ********************************************************************************* */
  public static double RelMass(final double Mass0, final double Vel0) {
    // http://www.softschools.com/formulas/physics/relativistic_mass_formula/546/
    // http://hyperphysics.phy-astr.gsu.edu/hbase/Relativ/tdil.html#c3
    double VSq = Vel0 * Vel0;
    double Factor = 1.0 / Math.sqrt(1.0 - (VSq / Globals.CSqared));
    double RelM = Mass0 * Factor;// mr = m0 / sqrt (1.0 – v2 / c2 )
    return RelM;
  }
  /* ********************************************************************************* */
  public static double RelGravTime(final double TimeFast, final double Mass, final double Distance) {// time dilation due to gravity
    // https://en.wikipedia.org/wiki/Gravitational_time_dilation
    double GravDist = (2.0 * Globals.GravConst * Mass) / (Distance * Globals.CSqared);
    double TimeFactor = Math.sqrt(1.0 - GravDist);
    double TimeSlow = TimeFast * TimeFactor;
    return TimeSlow;
  }
  /* ********************************************************************************* */
  static public void WritePng() {//throws Exception {
    try {//http://www.java2s.com/Code/Java/2D-Graphics-GUI/DrawanImageandsavetopng.htm
      int width = 200, height = 200;

      // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
      // into integer pixels
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D ig2 = bi.createGraphics();

      Font font = new Font("TimesRoman", Font.BOLD, 20);
      ig2.setFont(font);
      String message = "www.java2s.com!";
      FontMetrics fontMetrics = ig2.getFontMetrics();
      int stringWidth = fontMetrics.stringWidth(message);
      int stringHeight = fontMetrics.getAscent();
      ig2.setPaint(Color.black);
      ig2.drawString(message, (width - stringWidth) / 2, height / 2 + stringHeight / 4);

      //ig2.fillRect((int) this.XLoc, (int) this.YLoc, (int) this.Wdt, (int) this.Hgt);
      String FPath = ".\\";
      ImageIO.write(bi, "PNG", new File(FPath + "yourImageName.PNG"));
      ImageIO.write(bi, "JPEG", new File(FPath + "yourImageName.JPG"));
      ImageIO.write(bi, "gif", new File(FPath + "yourImageName.GIF"));
      ImageIO.write(bi, "BMP", new File(FPath + "yourImageName.BMP"));

    } catch (IOException ie) {
      ie.printStackTrace();
    }

  }
}

/*

 http://hyperphysics.phy-astr.gsu.edu/hbase/Relativ/einvel2.html
 http://hyperphysics.phy-astr.gsu.edu/hbase/Relativ/einvel.html#c1

 U = (Vb-Va) / ( 1.0 - ( (Vb*Va)/(C*C) ) )
 Where U is relative velocity as seen by A or B. 

 non-parallel
 http://math.ucr.edu/home/baez/physics/Relativity/SR/velocity.html

 https://www.britannica.com/science/relativistic-mass
 (u-v).(u-v) - (u × v)2/c2
 w2 =    -------------------------
 (1 - (u.v)/c2)2

 Vdiff = V0.subtract(V1);
 double dotdiff = Vdiff.dotprod(Vdiff);
 Vdot = V0.dotprod(V1);
 Vcross = V0.cross(V1);// want squared instead

 http://www.softschools.com/formulas/physics/relativistic_mass_formula/546/
 mr = m0 / sqrt (1 – v2 / c2 )

 CSq = C*C;
 mr = m0 / sqrt (1.0 – v2 / c2 )

 https://phys.libretexts.org/Bookshelves/University_Physics/Book%3A_University_Physics_(OpenStax)/Map%3A_University_Physics_III_-_Optics_and_Modern_Physics_(OpenStax)/5%3A__Relativity/5.6%3A_Relativistic_Velocity_Transformation
 http://hyperphysics.phy-astr.gsu.edu/hbase/Relativ/veltran.html

 //U = (Vb-Va) / ( 1.0 - ( (Vb*Va)/(C*C) ) )
 //Where U is relative velocity as seen by A or B. 

 
 https://en.wikipedia.org/wiki/Gravitational_time_dilation
 TimeSlow = TimeFast * sqrt( 1 - (  2*G*M/  (r*CSq) ) )

 so the first big question is, when two masses at different distances affect me, how do we combine them? 

 short term goal:
 create a MassOverDistance field

 */
