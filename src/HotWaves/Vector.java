/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HotWaves;

import static HotWaves.CellBase.NDims;
import java.awt.Color;

/**
 *
 * @author MultiTool
 */
public class Vector {
  public int VectNDims = 2;
  protected double[] data = null;

  // this stuff is just for drawing
  public double XLoc = 0, YLoc = 0, Wdt = 10, Hgt = 10;// for drawing
  public double XCtr = 0.0, YCtr = 0.0;// for drawing

  public Vector() {
    this(CellBase.NDims);
  }
  public Vector(int NDims0) {
    this.VectNDims = NDims0;
    data = new double[VectNDims];
  }
  public double Get(int Dex) {
    return this.data[Dex];
  }
  public void Set(int Dex, double Value) {
    this.data[Dex] = Value;
  }
  public void AssignDrawingProperties(double XLoc0, double YLoc0, double Wdt0, double Hgt0) {
    this.XLoc = XLoc0;
    this.YLoc = YLoc0;
    this.Wdt = Wdt0;
    this.Hgt = Hgt0;
    this.XCtr = this.XLoc + (this.Wdt / 2.0);
    this.YCtr = this.YLoc + (this.Hgt / 2.0);
  }
  public void Copy_From(Vector other) {
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      this.data[cnt] = other.data[cnt];
    }
  }
  public void Add(Vector other) {
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      this.data[cnt] += other.data[cnt];
    }
  }
  public void Subtract(Vector other) {// same as multiply other by -1, then add
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      this.data[cnt] -= other.data[cnt];
    }
  }
  public void Multiply(double Factor) {
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      this.data[cnt] *= Factor;
    }
  }
  public double Magnitude() {
    double SumSq = 0.0;// return absolute length of vector, pythagoras
    double Value;
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      Value = this.data[cnt];
      SumSq += Value * Value;
    }
    return Math.sqrt(SumSq);
  }
  public void Normalize() {// scale vector to length 1.0
    double Mag = this.Magnitude();
    if (Math.abs(Mag) < Globals.Fudge) {
      Mag = Globals.Fudge * Math.signum(Mag);
    }
    this.Multiply(1.0 / Mag);
  }
  public void Clear() {
    this.Fill(0.0);
  }
  public void Fill(double Value) {
    for (int cnt = 0; cnt < this.VectNDims; cnt++) {
      this.data[cnt] = Value;
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(DrawingContext ParentDC) {// IDrawable
    Color col = Color.orange;
    ParentDC.gr.setColor(col);
    double hypot = this.Magnitude();
    double[] PlotVect = new double[VectNDims];
    if (Math.abs(hypot) > 0.00001) {// normalize length
      for (int DimCnt = 0; DimCnt < VectNDims; DimCnt++) {
//        PlotVect[DimCnt] = this.data[DimCnt] / hypot;// normalized 
        PlotVect[DimCnt] = this.data[DimCnt] * 1.0;//5.0;
      }
      double len = this.Wdt / 2.0;
      ParentDC.gr.drawLine((int) (this.XCtr), (int) (this.YCtr), (int) (this.XCtr + PlotVect[0] * len), (int) (this.YCtr + PlotVect[1] * len));
    }
    ParentDC.gr.fillOval((int) (this.XCtr - 2), (int) (this.YCtr - 2), (int) (4), (int) (4));
  }
}
